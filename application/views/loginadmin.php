<?php
$error=$this->uri->segment(3);
?>

<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>SIGANI</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <!-- Bootstrap 3.3.7 -->
  <link rel="stylesheet" href="<?php echo base_url();?>assets/plugins/bootstrap/dist/css/bootstrap.min.css">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="<?php echo base_url();?>assets/plugins/font-awesome/css/font-awesome.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="<?php echo base_url();?>assets/plugins/Ionicons/css/ionicons.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="<?php echo base_url();?>assets/dist/css/AdminLTE.min.css">
  <!-- iCheck -->
  <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->

  <!-- Google Font -->
  <link rel="stylesheet" href="<?php echo base_url();?>assets/dist/css/font.css">
  <script>
          function togglePasswordVisibility() {
              var passwordField = document.getElementById("password");
              var passwordToggle = document.getElementById("passwordToggle");

              if (passwordField.type === "password") {
                  passwordField.type = "text";
                  passwordToggle.textContent = "Hide Password";
              } else {
                  passwordField.type = "password";
                  passwordToggle.textContent = "Show Password";
              }
          }
      </script>
</head>
<body class="hold-transition login-page">
<div class="login-box">
  <div class="login-logo">
    <img width="250px" src="<?php echo base_url();?>assets/image/logo_sigani.png" alt="SIGANI"><br>
  </div>
  <!-- /.login-logo -->
  <div class="login-box-body">
    <p class="login-box-msg"><span style="color: #777;font-size: 16px;"> Silahkan masuk menggunakan akun yang Anda miliki.</span></p>
    <?php
    if($error=='error')
    {
      ?>
      <div style="color: red;font-size: 12px;" align="center">Kombinasi Pengguna dan Sandi yang Anda masukkan salah, silahkan ulangi kembali</div>
      <br>
      <?php
    }
    ?>
    <form action="<?php echo site_url('LoginAdmin_Controller/aksi_login');?>" method="POST">
      <div class="form-group has-feedback">
        <input type="nip" name="nip" class="form-control" placeholder="Email">
        <span class="fa fa-user form-control-feedback"></span>
      </div>
      <div class="form-group has-feedback">
        <input type="password" class="form-control" name="password" placeholder="Sandi" id="password">
        <span class="glyphicon glyphicon-lock form-control-feedback"></span>
        <div align="right" style="margin-top: 5px"><button type="button" id="passwordToggle" onclick="togglePasswordVisibility()">Show Password</button></div>
      </div>
          <?php
          $salah = '';
          if($this->session->flashdata('salah') !== null)
          {
            $slh = $this->session->flashdata('salah');
          }else{
            $slh = ($salah != '') ? $salah : '';   
          }
          ?>
      <div align="center" style="margin-bottom: 5px"><font style="color:red"><?= $slh ?></font></div>
      <div class="form-group has-feedback">
        <input type="submit" class="btn btn-primary" value="Masuk" style="width: 100%;">
        <div style="margin-top: 10px"><a href="<?php echo site_url('Pendaftaran/page/daftar_user');?>" class="btn btn-warning" style="width: 100%; margin-right: 5px"></i> Daftar Akun</a>
        <!--<a href="<?php echo site_url('Cekskor/page/cek');?>" class="btn btn-info" style="width: 49%"></i> Cek Hasil Skor</a></div>-->
      </div>
    </form>
<center><span style="font-size: 10px;color: #777;">&copy 2023 Magistech Cipta Digital</span></center>
    <!-- /.social-auth-links -->
  </div>
  <!-- /.login-box-body -->
</div>
<!-- /.login-box -->

<!-- jQuery 3 -->
<script src="<?php echo base_url();?>assets/plugins/jquery/dist/jquery.min.js"></script>
<!-- Bootstrap 3.3.7 -->
<script src="<?php echo base_url();?>assets/plugins/bootstrap/dist/js/bootstrap.min.js"></script>
<!-- iCheck -->
</body>
</html>
