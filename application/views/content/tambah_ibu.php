<?php
$tahun = date("Y");
$t_awal = $tahun-1;
$awalan = $t_awal."/".$tahun;

$id=$this->uri->segment(4);
if($id!='')
{
  $data_sudah=$this->Sop_Model->qw("*","data_ibu","WHERE id>='$id'"
)->row_array();
  $am = 'Ubah';
  $edit = '1';
}else{
  $am = 'Tambah';
  $edit = '0';
}
$data_bidan=$this->Sop_Model->qw("*","master_bidan","ORDER BY nama_bidan ASC")->result();
$data_dokter=$this->Sop_Model->qw("*","master_dokter","ORDER BY nama_dokter ASC")->result();
$open = 'Sop_Controller/simpan_ibu';
$kembali = 'Sop_Controller/page/data_ibu';
        ?>
        <section class="content-header">
          <h1>
            <?php echo $am?> Ibu
          </h1>
          <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><a href="#">Form</a></li>
            <li class="active"><?php echo $am?> Ibu</li>
          </ol>
        </section>

        <section class="content">
          <div class="row">
            <div class="col-md-12">
              <!-- general form elements -->
              <div class="box box-primary">
                <form role="form" class="form-horizontal" action="<?php echo site_url($open);?>" method="POST" enctype="multipart/form-data">
                  <input name="edit" value="<?php echo $edit?>" type="hidden">
                  <div class="box-body">
                    <div class="row">
                      <div class="col-md-6">
                        <div class="form-group">
                          <label class="col-sm-3">Nama Anak</label>
                          <div class="col-sm-9">
                            <input name="nama_ibu" class="form-control">
                          </div>
                        </div>
                        <div class="form-group">
                          <label class="col-sm-3">Nama Orangtua</label>
                          <div class="col-sm-9">
                            <input name="nama_suami" class="form-control">
                          </div>
                        </div>
                        <div class="form-group">
                          <label class="col-sm-3">Tanggal Lahir</label>
                          <div class="col-sm-9">
                            <input name="umur" class="form-control" type="date">
                          </div>
                        </div>
                        <div class="form-group">
                          <label class="col-sm-3">Bidan</label>
                          <div class="col-sm-9">
                            <input name="bidan" class="form-control">
                          </div>
                        </div>
                        <div class="form-group">
                          <label class="col-sm-3">Dokter</label>
                          <div class="col-sm-9">
                            <input name="dokter" class="form-control">
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div class="box-footer">
                    <button type="submit" name="" class="btn btn-primary"><i class="fa fa-save"></i> Simpan</button>
                    <a href="<?php echo site_url($kembali);?>" class="btn btn-danger"><i class="fa fa-close"></i> Batal</a>
                  </div>
                </form>
              </div>
              <!-- /.box -->

              <!-- Form Element sizes -->

              <!-- /.box -->


              <!-- /.box -->

              <!-- Input addon -->

              <!-- /.box -->
            </div>
          </div>
        </section>