<?php
$tahun = date("Y");
$t_awal = $tahun-1;
$awalan = $t_awal."/".$tahun;

$id=$this->uri->segment(4);
$dibu=$this->uri->segment(5);
$data_screening=$this->Sop_Model->qw("*","data_screening","WHERE id='$id'")->row_array();
$id_ibu = $data_screening['id_ibu'];

if($id!='')
{
  $data_sudah=$this->Sop_Model->qw("*","data_screening","WHERE id>='$id'"
)->row_array();
  $am = 'Ubah';
  $edit = '1';
}else{
  $am = 'Tambah';
  $edit = '0';
}
$data_pernyataan=$this->Sop_Model->qw("*","pernyataan","ORDER BY id ASC")->result();
$data_dokter=$this->Sop_Model->qw("*","master_dokter","ORDER BY nama_dokter ASC")->result();
$data_ibu=$this->Sop_Model->qw("*","data_ibu","WHERE id='$id_ibu'")->row_array();
$open = 'Sop_Controller/simpan_screening';
if(isset($dibu))
  $kembali = 'Sop_Controller/page/histori_screening/'.$dibu;
else
  $kembali = 'Sop_Controller/page/data_ibu';

function inter($id_screening,$tipe){
  $CI =& get_instance();
  $ambil_jawaban=$CI->Sop_Model->qw("data_jawaban.jawaban","data_jawaban, pernyataan","WHERE data_jawaban.id_screening='$id_screening' AND data_jawaban.pertanyaan=pernyataan.id AND pernyataan.tipe='$tipe' AND data_jawaban.jawaban='1'")->num_rows();
  if($ambil_jawaban=='0')
    return "N";
  else
    return "A";
}
        ?>
        <section class="content-header">
          <h1>
            Interpretasi
          </h1>
          <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><a href="#">Form</a></li>
            <li class="active">Interpretasi</li>
          </ol>
        </section>

        <section class="content">
          <div class="row">
            <div class="col-md-12">
              <!-- general form elements -->
              <div class="box box-primary">
                <form role="form" class="form-horizontal" action="<?php echo site_url($open);?>" method="POST" enctype="multipart/form-data">
                  <input name="edit" value="<?php echo $edit?>" type="hidden">
                  <input name="id_ibu" value="<?php echo $id?>" type="hidden">
                  <div class="box-body">
                    <div class="row">
                      <div class="col-md-12">
                        <div class="form-group">
                          <label class="col-sm-2">Nama Ibu / Suami</label>
                          <div class="col-sm-5">
                            <?php echo $data_ibu['nama_ibu']?> / <?php echo $data_ibu['nama_suami']?>
                          </div>
                        </div>
                        <div class="form-group">
                          <label class="col-sm-2">Umur Anak</label>
                          <div class="col-sm-5">
                            <?php 

                            $dob = new DateTime($data_ibu['umur']);
                             
                            // Get current date
                            $now = new DateTime();
                             
                            // Calculate the time difference between the two dates
                            $diff = $now->diff($dob);
                            $umur = $diff->y." tahun ".$diff->m." bulan";
                            echo $umur;
                            ?>
                          </div>
                        </div>
                        <div class="form-group">
                          <label class="col-sm-2">Bidan / Dokter</label>
                          <div class="col-sm-5">
                            <?php
                            // $CI =& get_instance();
                            // $nama_bidan=$this->Sop_Model->qw("*","master_bidan","WHERE id='$data_ibu[bidan]'")->row_array();
                            // $bidan = $nama_bidan['nama_bidan'];
                            // echo $bidan;

                            // echo " / ";

                            // $CI =& get_instance();
                            // $nama_dokter=$this->Sop_Model->qw("*","master_dokter","WHERE id='$data_ibu[dokter]'")->row_array();
                            // $dokter = $nama_dokter['nama_dokter'];
                            // echo $dokter;

                            echo $data_ibu['bidan'];
                            echo " / ";
                            echo $data_ibu['dokter']
                            ?>
                          </div>
                        </div>
                        <div class="form-group">
                          <label class="col-sm-2">Tempat Screening</label>
                          <div class="col-sm-5">
                            <?php echo $data_screening['tempat']?>
                          </div>
                        </div>
                        <div class="form-group">
                          <label class="col-sm-2">Tanggal Screening</label>
                          <div class="col-sm-2">
                            <?php echo date("d-m-Y",strtotime($data_screening['tanggal']))?>
                          </div>
                        </div>
                        <div class="form-group">
                          <label class="col-sm-2">Berat (kg) / Tinggi (cm)</label>
                          <div class="col-sm-2">
                            <?php echo $data_screening['berat']?> kg / <?php echo $data_screening['tinggi']?> cm
                          </div>
                        </div>
                        <div class="form-group">
                          <label class="col-sm-2">Hasil</label>
                          <div class="col-sm-9">
                            <?php
                            $hasil_1 = inter($id,1);
                            $hasil_2 = inter($id,2);
                            $hasil_3 = inter($id,3);

                            $data=$this->Sop_Model->qw("*","master_inter","WHERE app='$hasil_1' AND work='$hasil_2' AND cir='$hasil_3'")->row_array();
                            ?>
                            <img width="250px" src="<?php echo base_url();?>assets/image/<?php echo $data['interpretasi']?>">
                            <br><br>
                            <b>Status : </b><?php echo $data['status']?>
                            <br><br>
                            <b>Prioritas Manajemen : </b><?php echo $data['prioritas']?>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div class="box-footer">
                    <!-- <button type="submit" name="" class="btn btn-primary"><i class="fa fa-save"></i> Simpan</button> -->
                    <a href="<?php echo site_url($kembali);?>" class="btn btn-danger"></i> Kembali</a>
                  </div>
                </form>
              </div>
              <!-- /.box -->

              <!-- Form Element sizes -->

              <!-- /.box -->


              <!-- /.box -->

              <!-- Input addon -->

              <!-- /.box -->
            </div>
          </div>
        </section>