<section class="content-header">
      <h1>
        Dashboard
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Dashboard</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <!-- Small boxes (Stat box) -->
      <div class="row">
        <?php
        if($this->session->userdata('level')=='kader')
        {
          $kode_kader = $this->session->userdata('nama');
        ?>
        <div class="col-lg-3 col-xs-6">
          <!-- small box -->
          <div class="small-box bg-aqua">
            <div class="inner">
              <?php
                $id_kader = $this->session->userdata('nama');
                $us=$this->Sop_Model->qw("odha.*","odha, kader","WHERE kader.nik=odha.nik_kader AND kader.kode_kader='$id_kader'","")->num_rows();
              ?>
              <h3><?php echo $us;?></h3>

              <p>Total ODHA Terdaftar</p>
            </div>
            <div class="icon">
              <i class="ion ion-bag"></i>
            </div>
            <a href="<?php echo site_url("Sop_Controller/page/lihat_odha"); ?>" class="small-box-footer">Lihat <i class="fa fa-arrow-circle-right"></i></a>
          </div>
        </div>
        <div class="col-lg-3 col-xs-6">
          <!-- small box -->
          <div class="small-box bg-green">
            <div class="inner">
              <?php
                $art=$this->Sop_Model->qw("plot_obat.*, odha.nik, odha.nama","plot_obat, odha, kader","
                WHERE plot_obat.jam <= NOW() + INTERVAL 15 MINUTE
                AND plot_obat.jam >= NOW()
                AND kader.nik=odha.nik_kader
                AND plot_obat.id_odha=odha.id
                AND kader.kode_kader='$id_kader'
                ORDER BY plot_obat.jam ASC")->num_rows();
              ?>
              <h3><?php echo $art;?></h3>

              <p>Total Reminder Obat ART</p>
            </div>
            <div class="icon">
              <i class="ion ion-stats-bars"></i>
            </div>
            <a href="<?php echo site_url("Sop_Controller/page/reminder_art"); ?>" class="small-box-footer">Info Lebih Lanjut <i class="fa fa-arrow-circle-right"></i></a>
          </div>
        </div>
        <?php
        }else if ($this->session->userdata('level')=='admin')
        {
          ?>
        <div class="col-lg-3 col-xs-6">
          <!-- small box -->
          <div class="small-box bg-red">
            <div class="inner">
              <?php
                $pembimbing=$this->Sop_Model->qw("*","fk_penguji","","")->num_rows();
              ?>
              <h3><?php echo $pembimbing;?></h3>

              <p>Total Pembimbing</p>
            </div>
            <div class="icon">
              <i class="ion ion-stats-bars"></i>
            </div>
            <a href="<?php echo site_url("Sop_Controller/page/lihat_pembimbing"); ?>" class="small-box-footer">Lihat<i class="fa fa-arrow-circle-right"></i></a>
          </div>
        </div>
        <div class="col-lg-3 col-xs-6">
          <!-- small box -->
          <div class="small-box bg-green">
            <div class="inner">
              <?php
                $mahasiswa=$this->Sop_Model->qw("*","msmhs","","")->num_rows();
              ?>
              <h3><?php echo $mahasiswa;?></h3>

              <p>Total Mahasiswa</p>
            </div>
            <div class="icon">
              <i class="ion ion-stats-bars"></i>
            </div>
            <a href="<?php echo site_url("Sop_Controller/page/lihat_mahasiswa"); ?>" class="small-box-footer">Lihat <i class="fa fa-arrow-circle-right"></i></a>
          </div>
        </div>
          <?php
        }else if ($this->session->userdata('level')=='odha')
        {
          ?>
          <div class="col-lg-3 col-xs-6">
          <!-- small box -->
          <div class="small-box bg-aqua">
            <div class="inner">
              <?php
                $id_odha = $this->session->userdata('nama');
                // /echo $id_odha;
                $data_odha=$this->Sop_Model->qw("odha.nik_kader","odha","WHERE kode_odha='$id_odha'")->row_array();
                $us=$this->Sop_Model->qw("nama","kader","
                  WHERE nik='$data_odha[nik_kader]'")->row_array();
              ?>
              <h4><?php echo $us['nama'];?></h4>

              <p>Nama Kader</p>
            </div>
            <div class="icon">
              <i class="ion ion-bag"></i>
            </div>
          </div>
        </div>
        <div class="col-lg-3 col-xs-6">
          <!-- small box -->
          <div class="small-box bg-red">
            <div class="inner">
              <?php
                $lab=$this->Sop_Model->qw("lab.*, odha.nik, odha.nama","lab, odha","
                WHERE lab.waktu_berikutnya <= NOW() + INTERVAL 3 DAY
                AND lab.waktu_berikutnya >= NOW()
                AND lab.id_odha=odha.id
                AND odha.kode_odha='$id_odha'
                ORDER BY lab.waktu_berikutnya ASC")->num_rows();
              ?>
              <h3><?php echo $lab;?></h3>

              <p>Total Reminder Uji Laboratorium</p>
            </div>
            <div class="icon">
              <i class="ion ion-stats-bars"></i>
            </div>
            <a href="<?php echo site_url("Sop_Controller/page/reminder_lab"); ?>" class="small-box-footer">Info Lebih Lanjut <i class="fa fa-arrow-circle-right"></i></a>
          </div>
        </div>
        <div class="col-lg-3 col-xs-6">
          <!-- small box -->
          <div class="small-box bg-green">
            <div class="inner">
              <?php
                $art=$this->Sop_Model->qw("plot_obat.*, odha.nik, odha.nama","plot_obat, odha","
                WHERE plot_obat.jam <= NOW() + INTERVAL 15 MINUTE
                AND plot_obat.jam >= NOW()
                AND plot_obat.id_odha=odha.id
                AND odha.kode_odha='$id_odha'
                ORDER BY plot_obat.jam ASC")->num_rows();
              ?>
              <h3><?php echo $art;?></h3>

              <p>Total Reminder Obat ART</p>
            </div>
            <div class="icon">
              <i class="ion ion-stats-bars"></i>
            </div>
            <a href="<?php echo site_url("Sop_Controller/page/reminder_art"); ?>" class="small-box-footer">Info Lebih Lanjut <i class="fa fa-arrow-circle-right"></i></a>
          </div>
        </div>
          <?php
        }
        ?>
        <!-- ./col -->
        
        <!-- ./col -->
        <!-- ./col -->
      </div>
      <!-- /.row -->
      <!-- Main row -->
      <!-- /.row (main row) -->

    </section>