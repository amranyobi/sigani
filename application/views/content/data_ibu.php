
<?php
$berhasil=$this->uri->segment(5);
$email = $this->session->userdata('email');
$id_user = $this->session->userdata('id_user');
$date = date("Y-m-d");

//data ujian
// $ujian=$this->Sop_Model->qw("t_penjadwalan.*, t_kat_soal.kategori","t_kat_soal, t_penjadwalan","WHERE t_kat_soal.id=t_penjadwalan.tipe_ujian AND t_penjadwalan.tanggal>='$date' AND t_penjadwalan.status!='3' AND t_penjadwalan.flag='0' ORDER BY t_penjadwalan.tanggal DESC, t_penjadwalan.waktu_mulai DESC")->result();

$info_user = $this->Sop_Model->qw("*","user","WHERE id_user='$id_user'")->row_array();
$id_user = $info_user['id_user'];

$data_ibu=$this->Sop_Model->qw("*","data_ibu","WHERE id_user='$id_user'")->result();

//hampus semua
$this->db->from('log_input');
$this->db->truncate();

// //cek pernah ujian
// $cek_ujian = $this->Sop_Model->qw("t_peserta_ujian.score","t_peserta_ujian, tblsiswa","WHERE t_peserta_ujian.id_peserta=tblsiswa.id_siswa AND tblsiswa.nis='$email' ORDER BY t_peserta_ujian.id_peserta_ujian DESC");
// $hitung_ujian = $cek_ujian->num_rows();
// $hasil_ujian = $cek_ujian->row_array();
// $skor_hasil = $hasil_ujian['score'];

// if($berhasil!='')
// {
//     $kata = "Tambah";
//     $call = "success";
// }


?>
<section class="content-header">

      <h1>
        Data Anak
      </h1>

      <ol class="breadcrumb">

        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>

        <li><a href="#">Data</a></li>

        <li class="active">Anak</li>

      </ol>

</section>



   <section class="content">

      <div class="row">

        <div class="col-xs-12">

          <div class="box box-primary">
            <?php
            if(isset($berhasil))
            {
              ?>
              <br>
              <div class="callout callout-<?php echo $call?>" style="margin-left: 10px; margin-right: 10px">
              <h4>Tambah Anak Berhasil</h4>
              <p>
                Tambah Anak berhasil dilakukan, Selanjutnya inputkan data Anak.
              </p>
              </div>
              <?php
            }
            ?>
            <div class="box-header" style="margin-top: 20px;">
              <div align="right" style="margin-right: 20px">
                <a href="<?php echo site_url("Sop_Controller/page/tambah_ibu"); ?>" class="btn btn-sm btn-success"><i class="fa fa-plus"></i> Tambah Anak</a>
                <!--s<a href="<?php echo site_url("Sop_Controller/cetak_laporan"); ?>" class="btn btn-sm btn-primary" target="_blank"><i class="fa fa-file-excel-o"></i> Export</a>-->
              </div>
            </div>
            <div class="box-body">

              <table id="example4" class="table table-bordered table-striped display">

                <thead>

                <tr>

                  <th>No</th>
                  <th>Nama Anak /<br>Nama Orangtua</th>
                  <th>Umur</th>
                  <th>Bidan / Dokter</th>
                  <th>Screening</th>

                </tr>

                </thead>

                <tbody>

                <?php

                  $no=0;

                  foreach($data_ibu as $tampil){

                  $no++;

                ?>

                <tr>

                  <td><?php echo $no;?></td>
                  <td><?php echo $tampil->nama_ibu?> /<br><?php echo $tampil->nama_suami?></td>
                  <td><?php
                  // Create a datetime object using date of birth
                  $dob = new DateTime($tampil->umur);
                   
                  // Get current date
                  $now = new DateTime();
                   
                  // Calculate the time difference between the two dates
                  $diff = $now->diff($dob);
                  $umur = $diff->y." tahun ".$diff->m." bulan";
                  echo $umur?></td>
                  <td>
                    <?php
                    // $CI =& get_instance();
                    // $nama_bidan=$this->Sop_Model->qw("*","master_bidan","WHERE id='$tampil->bidan'")->row_array();
                    // $bidan = $nama_bidan['nama_bidan'];
                    // echo $bidan;
                    // echo " / ";
                    // $CI =& get_instance();
                    // $nama_dokter=$this->Sop_Model->qw("*","master_dokter","WHERE id='$tampil->dokter'")->row_array();
                    // $dokter = $nama_dokter['nama_dokter'];
                    // echo $dokter;
                    echo $tampil->bidan;
                    echo " / ";
                    echo $tampil->dokter;
                    ?>
                  </td>
                  <td>
                    <a href="<?php echo site_url("Sop_Controller/page/histori_screening/".$tampil->id); ?>" class="btn btn-sm btn-warning"><i class="fa fa-bars"></i> Histori</a>
                    <a href="<?php echo site_url("Sop_Controller/page/tambah_screening/".$tampil->id); ?>" class="btn btn-sm btn-success"><i class="fa fa-plus"></i> Tambah</a>
                  </td>

                </tr>

                <?php } ?>

                </tbody>

              </table>

            </div>
            

            

            <!-- /.box-body -->

          </div>

          <!-- /.box -->

        </div>

        <!-- /.col -->

      </div>

      <!-- /.row -->

    </section>