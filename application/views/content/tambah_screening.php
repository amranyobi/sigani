<?php
$tahun = date("Y");
$t_awal = $tahun-1;
$awalan = $t_awal."/".$tahun;

$id=$this->uri->segment(4);
if($id!='')
{
  $data_sudah=$this->Sop_Model->qw("*","data_screening","WHERE id>='$id'"
)->row_array();
  $am = 'Ubah';
  $edit = '1';
}else{
  $am = 'Tambah';
  $edit = '0';
}
$data_pernyataan=$this->Sop_Model->qw("*","pernyataan","ORDER BY id ASC")->result();
$data_dokter=$this->Sop_Model->qw("*","master_dokter","ORDER BY nama_dokter ASC")->result();
$data_ibu=$this->Sop_Model->qw("*","data_ibu","WHERE id='$id'")->row_array();

//hampus semua
$this->db->from('log_input');
$this->db->truncate();
$waktu = date("Y-m-d H:i:s");
$ary=array(
  'waktu' =>$waktu
);
$this->Sop_Model->simpan_log('log_input',$ary);

$open = 'Sop_Controller/simpan_screening';
$kembali = 'Sop_Controller/page/data_ibu';
        ?>
        <section class="content-header">
          <h1>
            <?php echo $am?> Screening
          </h1>
          <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><a href="#">Form</a></li>
            <li class="active"><?php echo $am?> Ibu</li>
          </ol>
        </section>

        <section class="content">
          <div class="row">
            <div class="col-md-12">
              <!-- general form elements -->
              <div class="box box-primary">
                <form role="form" class="form-horizontal" action="<?php echo site_url($open);?>" method="POST" enctype="multipart/form-data">
                  <input name="edit" value="<?php echo $edit?>" type="hidden">
                  <input name="id_ibu" value="<?php echo $id?>" type="hidden">
                  <div class="box-body">
                    <div class="row">
                      <div class="col-md-12">
                        <div class="form-group">
                          <label class="col-sm-2">Nama Anak / Nama Orangtua</label>
                          <div class="col-sm-5">
                            <?php echo $data_ibu['nama_ibu']?> / <?php echo $data_ibu['nama_suami']?>
                          </div>
                        </div>
                        <div class="form-group">
                          <label class="col-sm-2">Umur Anak</label>
                          <div class="col-sm-5">
                            <?php
                            $dob = new DateTime($data_ibu['umur']);
                             
                            // Get current date
                            $now = new DateTime();
                             
                            // Calculate the time difference between the two dates
                            $diff = $now->diff($dob);
                            $umur = $diff->y." tahun ".$diff->m." bulan";
                            echo $umur;
                            ?>
                          </div>
                        </div>
                        <!-- <div class="form-group">
                          <label class="col-sm-2">Umur Bayi</label>
                          <div class="col-sm-5">
                            <?php echo $data_ibu['umur_kehamilan']?> bulan
                          </div>
                        </div> -->
                        <div class="form-group">
                          <label class="col-sm-2">Bidan / Dokter</label>
                          <div class="col-sm-5">
                            <?php
                            // $CI =& get_instance();
                            // $nama_bidan=$this->Sop_Model->qw("*","master_bidan","WHERE id='$data_ibu[bidan]'")->row_array();
                            // $bidan = $nama_bidan['nama_bidan'];
                            // echo $bidan;

                            // echo " / ";

                            // $CI =& get_instance();
                            // $nama_dokter=$this->Sop_Model->qw("*","master_dokter","WHERE id='$data_ibu[dokter]'")->row_array();
                            // $dokter = $nama_dokter['nama_dokter'];
                            // echo $dokter;
                            echo $data_ibu['bidan'];
                            echo " / ";
                            echo $data_ibu['dokter'];
                            ?>
                          </div>
                        </div>
                        <div class="form-group">
                          <label class="col-sm-2">Tempat Periksa</label>
                          <div class="col-sm-5">
                            <input name="tempat" class="form-control" type="text">
                          </div>
                        </div>
                        <div class="form-group">
                          <label class="col-sm-2">Tanggal Periksa</label>
                          <div class="col-sm-2">
                            <input name="tanggal" class="form-control" type="date">
                          </div>
                        </div>
                        <!-- <div class="form-group">
                          <label class="col-sm-2">Waktu Periksa</label>
                          <div class="col-sm-4">
                            <select name="jam">
                            <?php
                            for($x=0;$x<=23;$x++)
                            {
                              ?>
                              <option value="<?php echo sprintf("%02d", $x)?>"><?php echo sprintf("%02d", $x)?></option>
                              <?php
                            }
                            ?>
                            </select>
                            <select name="menit">
                            <?php
                            for($y=1;$y<=59;$y++)
                            {
                              ?>
                              <option value="<?php echo sprintf("%02d", $y)?>"><?php echo sprintf("%02d", $y)?></option>
                              <?php
                            }
                            ?>
                            </select>
                          </div>
                        </div> -->
                        <div class="form-group">
                          <label class="col-sm-2">Berat Badan (kg)</label>
                          <div class="col-sm-2">
                            <input name="berat" class="form-control" type="text">
                          </div>
                        </div>
                        <div class="form-group">
                          <label class="col-sm-2">Tinggi Badan (cm)</label>
                          <div class="col-sm-2">
                            <input name="tinggi" class="form-control" type="text">
                          </div>
                        </div>
                        <div class="form-group">
                          <label class="col-sm-2">Screening</label>
                          <div class="col-sm-9">
                            <table border="0">
                            <?php
                            $no = 0;
                            foreach ($data_pernyataan as $pernyataan) {
                              $no++;
                              ?>
                              <tr>
                                <td valign="top"><?php echo $no?>.</td>
                                <td><div style="margin-left: 5px;"><?php echo $pernyataan->isi?></div></p></td>
                                <td><select name="jawaban[<?php echo $no?>]">
                                  <option value="1">Ya</option>
                                  <option value="0" selected>Tidak</option>
                                </select></td>
                              </tr>
                              <?php
                            }
                            ?>
                            </table>
                          </div>
                        </div>
                        <div class="form-group">
                          <label class="col-sm-2">Artikel Terkait Penilaian</label>
                          <div class="col-sm-8">
                            <!-- <div class="row"> -->
                              <iframe width="100%" src="https://www.youtube.com/embed/bJF-8-HQN9w?si=ZfXjbc_sRui-xFYD" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" allowfullscreen></iframe>

                              <iframe width="100%" src="https://www.youtube.com/embed/K8_j_aNI1ts?si=2EuDP_YSqSZso6iM" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" allowfullscreen></iframe>

                              <img width="100%" src="<?php echo base_url();?>assets/image/bantuan1.jpeg">
                              <br>
                              <img width="100%" src="<?php echo base_url();?>assets/image/bantuan3.jpeg">
                              <br>
                              <img width="100%" src="<?php echo base_url();?>assets/image/bantuan2.jpeg">
                            <!-- </div> -->
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div class="box-footer">
                    <button type="submit" name="" class="btn btn-primary"><i class="fa fa-save"></i> Simpan</button>
                    <a href="<?php echo site_url($kembali);?>" class="btn btn-danger"><i class="fa fa-close"></i> Batal</a>
                  </div>
                </form>
              </div>
              <!-- /.box -->

              <!-- Form Element sizes -->

              <!-- /.box -->


              <!-- /.box -->

              <!-- Input addon -->

              <!-- /.box -->
            </div>
          </div>
        </section>