<style>
  #showNama {
  display: none; // you're using diaplay!
}
</style>
<?php
$noreg=$this->uri->segment(4);
$getreg=$this->Sop_Model->qw("t_peserta_ujian.*, tblsiswa.*, t_penjadwalan.tanggal","t_peserta_ujian, tblsiswa, t_penjadwalan","WHERE t_peserta_ujian.id_peserta=tblsiswa.id_siswa AND t_penjadwalan.id_penjadwalan=t_peserta_ujian.id_penjadwalan AND t_peserta_ujian.no_reg='$noreg'")->row_array();
//header('Content-Type: application/json');
header('Access-Control-Allow-Origin: *');
?>
<section class="content-header">
      <h1>
        Cek Hasil Skor
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="#">Form</a></li>
        <li class="active">Cek Hasil Skor</li>
      </ol>
    </section>

    <section class="content">
      <div class="row">
        <div class="col-md-12">
          <!-- general form elements -->
          <div class="box box-primary">
            <form role="form" class="form-horizontal" action="" method="POST" enctype="multipart/form-data">
              <div class="box-body">
                <div class="row">
                  <div class="col-md-6">
                    <div class="form-group">
                      <label class="col-sm-3">No. Registrasi</label>
                      <div class="col-sm-6">
                        <?php echo $getreg['no_reg']?>
                      </div>
                    </div>
                    <div class="form-group">
                      <label class="col-sm-3">Nama</label>
                      <div class="col-sm-8">
                        <?php echo $getreg['nama'];?>
                      </div>
                    </div>
                    <?php
                        if($getreg['nim']!='')
                        {
                          ?>
                          <div class="form-group">
                            <label class="col-sm-3">NIM</label>
                            <div class="col-sm-6">
                              <?php echo $getreg['nim']?>
                            </div>
                          </div>
                          <?php
                        }
                    ?>
                    <div class="form-group">
                      <label class="col-sm-3">Email</label>
                      <div class="col-sm-6">
                        <?php echo $getreg['email']?>
                      </div>
                    </div>
                    <div class="form-group">
                      <label class="col-sm-3">Alamat</label>
                      <div class="col-sm-6">
                        <?php echo $getreg['alamat']?>
                      </div>
                    </div>
                    <div class="form-group">
                      <label class="col-sm-3">Tanggal Ujian</label>
                      <div class="col-sm-6">
                        <?php echo date("d-m-Y",strtotime($getreg['tanggal']))?>
                      </div>
                    </div>
                    <div class="form-group">
                      <label class="col-sm-3">Skor</label>
                      <div class="col-sm-6">
                        <?php echo $getreg['score']?>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              </div>
              </div>

              <div class="box-footer">
                <a href="<?php echo site_url('Cekskor/page/cek');?>" class="btn btn-info">Cek Nomor Lain</a>
                <a href="<?php echo site_url('');?>" class="btn btn-danger">Kembali</a>
              </div>
            </form>
          </div>
          <!-- /.box -->

          <!-- Form Element sizes -->
         
          <!-- /.box -->

          
          <!-- /.box -->

          <!-- Input addon -->
          
          <!-- /.box -->
        </div>
      </div>
    </section>
    <script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.8.3/jquery.min.js"></script>