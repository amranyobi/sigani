<?php
$tahun = date("Y");
$t_awal = $tahun-1;
$awalan = $t_awal."/".$tahun;

$id=$this->uri->segment(4);
$data_pernyataan=$this->Sop_Model->qw("*","pernyataan","ORDER BY id ASC")->result();
$data_dokter=$this->Sop_Model->qw("*","master_dokter","ORDER BY nama_dokter ASC")->result();
$data_ibu=$this->Sop_Model->qw("*","data_ibu","WHERE id='$id'")->row_array();
$data_screening=$this->Sop_Model->qw("*","data_screening","WHERE id_ibu='$id' ORDER BY id ASC")->result();
$open = 'Sop_Controller/simpan_screening';
$kembali = 'Sop_Controller/page/data_ibu';

function inter($id_screening,$tipe){
  $CI =& get_instance();
  $ambil_jawaban=$CI->Sop_Model->qw("data_jawaban.jawaban","data_jawaban, pernyataan","WHERE data_jawaban.id_screening='$id_screening' AND data_jawaban.pertanyaan=pernyataan.id AND pernyataan.tipe='$tipe' AND data_jawaban.jawaban='1'")->num_rows();
  if($ambil_jawaban=='0')
    return "N";
  else
    return "A";
}
        ?>
        <section class="content-header">
          <h1>
            Histori Screening
          </h1>
          <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><a href="#">Form</a></li>
            <li class="active">Histori Screening</li>
          </ol>
        </section>

        <section class="content">
          <div class="row">
            <div class="col-md-12">
              <!-- general form elements -->
              <div class="box box-primary">
                <form role="form" class="form-horizontal" action="<?php echo site_url($open);?>" method="POST" enctype="multipart/form-data">
                  <div class="box-body">
                    <div class="row">
                      <div class="col-md-12">
                        <div class="form-group">
                          <label class="col-sm-2">Nama Anak / Orangtua</label>
                          <div class="col-sm-5">
                            <?php echo $data_ibu['nama_ibu']?> / <?php echo $data_ibu['nama_suami']?>
                          </div>
                        </div>
                        <div class="form-group">
                          <label class="col-sm-2">Umur Anak</label>
                          <div class="col-sm-5">
                            <?php
                            $dob = new DateTime($data_ibu['umur']);
                             
                            // Get current date
                            $now = new DateTime();
                             
                            // Calculate the time difference between the two dates
                            $diff = $now->diff($dob);
                            $umur = $diff->y." tahun ".$diff->m." bulan";
                            echo $umur?>
                          </div>
                        </div>
                        <!-- <div class="form-group">
                          <label class="col-sm-2">Umur Bayi</label>
                          <div class="col-sm-5">
                            <?php echo $data_ibu['umur_kehamilan']?> bulan
                          </div>
                        </div> -->
                        <div class="form-group">
                          <label class="col-sm-2">Bidan / Dokter</label>
                          <div class="col-sm-5">
                            <?php
                            // $CI =& get_instance();
                            // $nama_bidan=$this->Sop_Model->qw("*","master_bidan","WHERE id='$data_ibu[bidan]'")->row_array();
                            // $bidan = $nama_bidan['nama_bidan'];
                            // echo $bidan;

                            // echo " / ";

                            // $CI =& get_instance();
                            // $nama_dokter=$this->Sop_Model->qw("*","master_dokter","WHERE id='$data_ibu[dokter]'")->row_array();
                            // $dokter = $nama_dokter['nama_dokter'];
                            // echo $dokter;
                            echo $data_ibu['bidan'];
                            echo " / ";
                            echo $data_ibu['dokter'];
                            ?>
                          </div>
                        </div>
                        <div class="form-group">
                          <label class="col-sm-2">Data Screening</label>
                          <div class="col-sm-9">
                            <table id="example4" class="table table-bordered table-striped display">

                              <thead>

                              <tr>

                                <th>No</th>
                                <th>Waktu / Tempat</th>
                                <th>Lama Input</th>
                                <th>Berat</th>
                                <th>Status</th>
                                <th>Detail</th>

                              </tr>

                              </thead>

                              <tbody>

                            <?php
                            $no = 0;
                            foreach ($data_screening as $scr) {
                              $no++;
                              ?>
                              <tr>
                                <td valign="top"><?php echo $no?>.</td>
                                <td><?php
                                echo date("d-m-Y",strtotime($scr->tanggal));
                                echo " / ";
                                echo $scr->tempat;
                                ?></td>
                                <td><?php echo $scr->waktu?></td>
                                <td><?php echo $scr->berat?> kg</td>
                                <td>
                                  <?php
                                  $hasil_1 = inter($scr->id,1);
                                  $hasil_2 = inter($scr->id,2);
                                  $hasil_3 = inter($scr->id,3);

                                  $data=$this->Sop_Model->qw("*","master_inter","WHERE app='$hasil_1' AND work='$hasil_2' AND cir='$hasil_3'")->row_array();

                                  echo $data['status'];
                                  ?>
                                </td>
                                <td><a href="<?php echo site_url("Sop_Controller/page/data_kesimpulan/".$scr->id."/".$id); ?>" class="btn btn-sm btn-success"> Detail</a></td>
                              </tr>
                              <?php
                            }
                            ?>
                              </tbody>
                            </table>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div class="box-footer">
                    <a href="<?php echo site_url($kembali);?>" class="btn btn-danger"></i> Kembali</a>
                  </div>
                </form>
              </div>
              <!-- /.box -->

              <!-- Form Element sizes -->

              <!-- /.box -->


              <!-- /.box -->

              <!-- Input addon -->

              <!-- /.box -->
            </div>
          </div>
        </section>