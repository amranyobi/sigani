<?php
class DigitalSign_Controller extends CI_Controller {
	function __construct(){
		parent:: __construct();
		$this->load->model('Sop_Model');
		$this->load->library('pdf');
		$this->load->helper(array('form', 'url'));
	}
	function page(){
		$page=$this->uri->segment(3);
		$data['page']=$page;
		if($page=="digitalsign"){
			$nip=$this->uri->segment(4);
			$data['data_pegawai'] = $this->get_pegawai($nip);
		}
		$this->load->view('qrcode',$data);
	}

	function get_pegawai($nip){
		  $url = 'https://simpeg.kendalkab.go.id/v17/api/pegawaidetail/'.$nip;
          $curl = curl_init();
          curl_setopt_array($curl, array(
              CURLOPT_RETURNTRANSFER => true,
              CURLOPT_ENCODING => "",
              CURLOPT_MAXREDIRS => 10,
              CURLOPT_TIMEOUT => 60,
              CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
              CURLOPT_CUSTOMREQUEST => "GET",
              CURLOPT_HTTPHEADER => array(
                  "content-type: application/x-www-form-urlencoded"
              ),
              CURLOPT_URL => $url
          ));

          $response = curl_exec($curl);
          $curl_errno = curl_errno($curl);
          $curl_error = curl_error($curl);
          curl_close($curl);
          $svcpegawai = json_decode($response, true);
          $coba = $svcpegawai['pegawaidetail'][0];
          return $coba;
	}

	
	
}
