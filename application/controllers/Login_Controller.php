<?php
class Login_Controller extends CI_Controller {
	function __construct(){
		parent:: __construct();
		$this->load->model('Login_Model');

		#SSO
        $this->sso_login = 'http://sisdm.semarangkota.go.id/api/sso_login';
        $this->sso_logout = 'http://sisdm.semarangkota.go.id/api/sso_logout';
        $this->sso_get_user = 'http://sisdm.semarangkota.go.id/api/user';
        $this->sso_get_pegawai = 'http://sisdm.semarangkota.go.id/api/sisop?nip='; #PARAMETER NIP
        
        #END SSO
	}
	function index(){
		$this->load->view('login');
	}
	function aksi_login(){
			$username=$this->input->post('nip');
			$password=$this->input->post('password');

			$par = [
                    'url'       => $this->sso_login,
                    'data'      => [
                                    'username'  => $username,
                                    'password'  => $password,
                                   ],
                   ];
            $res = $this->request_post($par);

            if(isset($res->status)){
            	if($res->status == 'Success')
	            {
	                $dt = ['_token' => $res->token];
	                $this->session->set_userdata($dt);

	                $par2 = ['url' => $this->sso_get_user];
	                $user = $this->request_get($par2);

	                $par3 = ['url' => $this->sso_get_pegawai.$username];
	                $pegawai = $this->request_get($par3);

	                $resx = new stdClass();

	                // $resx->id_pengguna          = $user->id;
	                $resx->nip                  = $pegawai->nip;
	                $resx->id_pengguna_group    = 2;
	                $resx->nama                 = $pegawai->nama;
	                $resx->golongan             = $pegawai->golongan;
	                $resx->rumpun_jabatan       = $pegawai->rumpun_jabatan;
	                $resx->jabatan       		= $pegawai->jabatan;
	                $resx->opd       			= $pegawai->opd;
	                $resx->kd_opd       		= $pegawai->kd_opd;
	                $resx->nama_user            = $user->name;

	                $sss = true;
	                $xyz = true;
	            }else{
	                $pegawai = $this->get_sisdm($username);

	                if(isset($pegawai->kedudukan_pegawai))
	                {
	                    if($pegawai->kedudukan_pegawai == 'Pensiun')
	                    {
	                        $this->session->set_flashdata('salah', 'Pegawai sudah tidak aktif');
	                        redirect('Login_Controller');
	                    }else{
	                        $this->session->set_flashdata('salah', 'NIP atau password salah');
	                        redirect('Login_Controller');
	                    }
	                }else{
	                    $this->session->set_flashdata('salah', 'NIP atau password salah');
	                    redirect('Login_Controller');
	                }
	            }
            }else
            {
            	$this->session->set_flashdata('salah', 'Server sibuk, silahkan coba beberapa saat lagi');
	            redirect('Login_Controller');
            }
            


			/*$where=array(
					'username' =>$username,
					'password' =>md5($password)
				);
			$cek=$this->Login_Model->cek_login('user',$where)->num_rows();*/
			if($xyz==true)
			{
				$query=$this->db->where('username',$username)->get('user');
				$hitung_user=$query->num_rows();

				if($hitung_user=='0')
				{
					$rumpun_jabatan =  $resx->rumpun_jabatan;
					$jabatan = $resx->jabatan;
					if($rumpun_jabatan=='Pelaksana' || $rumpun_jabatan=='Fungsional')
					{
						$ary=array(
							'id_user'	=>$username,
							'username'	=>$username,
							'password'	=>'',
							'level'		=>'1'
						);
						$this->Login_Model->simpan_user('user',$ary);
						$data_session=array(
							'nama'	=>$username,
							//'id_user'	=>$row->id_user,
							'status' => 'login',
							'level'=>'1',
							'nama_user' => $resx->nama_user,
							'nama_pegawai' => $resx->nama,
							'golongan' => $resx->golongan,
							'rumpun_jabatan' => $resx->rumpun_jabatan,
							'opd' => $resx->opd,
							'kd_opd' => $resx->kd_opd,
							'jabatan' => $resx->jabatan
							);
						$this->session->set_userdata($data_session);
						redirect('Sop_Controller/page/home');
					}elseif ($rumpun_jabatan=='Fungsional'){
						$this->session->set_flashdata('salah', 'Anda tidak memiliki hak akses');
                        redirect('Login_Controller');
					}else{
						$jabatan = $resx->jabatan;
						$pecah_jabatan = explode(' ', $jabatan); 
                  		$depan_jabatan = $pecah_jabatan[0]." ".$pecah_jabatan[1];
                  		if($jabatan=="Kepala")
                  		{
                  			$ary=array(
								'id_user'	=>$username,
								'username'	=>$username,
								'password'	=>'',
								'level'		=>'2'
							);
							$this->Login_Model->simpan_user('user',$ary);
							$data_session=array(
								'nama'	=>$username,
								//'id_user'	=>$row->id_user,
								'status' => 'login',
								'level'=>'2',
								'nama_user' => $resx->nama_user,
								'nama_pegawai' => $resx->nama,
								'golongan' => $resx->golongan,
								'rumpun_jabatan' => $resx->rumpun_jabatan,
								'opd' => $resx->opd,
								'kd_opd' => $resx->kd_opd,
								'jabatan' => $resx->jabatan
								);
							$this->session->set_userdata($data_session);
							redirect('Eselon_Controller/page/home');
                  		}else{
                  			if($depan_jabatan=='Kepala Sub' || $depan_jabatan=='Kepala Seksi')
                  			{
                  				$leveling = '4';
                  				$masuk = '1';
                  			}	
                  			elseif($depan_jabatan=='Kepala Bidang' || $depan_jabatan=='Kepala Bagian')
                  			{
                  				$leveling = '3';
                  				$masuk = '1';
                  			}

                  			if($masuk=='1')
                  			{
                  				$ary=array(
									'id_user'	=>$username,
									'username'	=>$username,
									'password'	=>'',
									'level'		=>$leveling
								);
								$this->Login_Model->simpan_user('user',$ary);
								$data_session=array(
									'nama'	=>$username,
									//'id_user'	=>$row->id_user,
									'status' => 'login',
									'level'=>$leveling,
									'nama_user' => $resx->nama_user,
									'nama_pegawai' => $resx->nama,
									'golongan' => $resx->golongan,
									'rumpun_jabatan' => $resx->rumpun_jabatan,
									'opd' => $resx->opd,
									'kd_opd' => $resx->kd_opd,
									'jabatan' => $resx->jabatan
									);
								$this->session->set_userdata($data_session);
								redirect('Eselon_Controller/page/home');
                  			}else{
                  				$this->session->set_flashdata('salah', 'Anda tidak memiliki hak akses');
                       			redirect('Login_Controller');
                  			}

                  		}
					}
				}else{
					$row=$query->row();
					$level=$row->level;
					//if($cek > 0){
					if($sss==true){
						$data_session=array(
							'nama'	=>$username,
							//'id_user'	=>$row->id_user,
							'status' => 'login',
							'level'=>$level,
							'nama_user' => $resx->nama_user,
							'nama_pegawai' => $resx->nama,
							'golongan' => $resx->golongan,
							'rumpun_jabatan' => $resx->rumpun_jabatan,
							'opd' => $resx->opd,
							'kd_opd' => $resx->kd_opd,
							'jabatan' => $resx->jabatan
							);
						$this->session->set_userdata($data_session);
						if($level=='1' || $level=='5')
							redirect('Sop_Controller/page/home');
						else
							redirect('Eselon_Controller/page/home');
					}else{
						redirect('Login_Controller');
					}
				}
				
			}
			
		}

	public function request_post($par)
    {
        $data = $par['data'];
         
        $data_string = '';
        foreach($data as $key => $value) { $data_string .= $key.'='.$value.'&'; }
        rtrim($data_string, '&');
        
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $par['url']);
        curl_setopt($ch, CURLOPT_POST, count($data));
        curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
        curl_setopt($ch, CURLOPT_HEADER, 0);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);

        $responseJson = curl_exec( $ch );

        curl_close($ch);

        $result = json_decode($responseJson);
        return $result;
    }

    public function request_get($par)
    {
        $data = array(
            'Content-Type: application/json',
            'Accept: application/json',
            'Authorization: Bearer ' . $this->session->userdata('_token'),
        );

        $ch = curl_init();
        curl_setopt( $ch, CURLOPT_URL, $par['url']);
        curl_setopt( $ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt( $ch, CURLOPT_FOLLOWLOCATION, 1);
        curl_setopt( $ch, CURLOPT_HTTPHEADER, $data);
        curl_setopt( $ch, CURLOPT_HEADER, 0);

        $result = curl_exec( $ch );

        curl_close($ch);

        if ($result === FALSE) {
            throw new Exception('CURL Error: ' . curl_error($ch), curl_errno($ch));
        }else{
            $obj = json_decode($result);
            return $obj;
        }
    }

	function logout(){
		$this->session->sess_destroy();
		redirect('Login_Controller');
	}

	public function get_sisdm($nipx = '')
    {
        $result_token   = $this->get_token();

        if($result_token['status'] == true)
        {
            $nip    = $nipx; 

            $url = 'http://sisdm.semarangkota.go.id/api/sisop?nip='. $nip;

            $data = array(
                'Content-Type: application/json',
                'Accept: application/json',
                'Authorization: '.$result_token['_token_type'].' ' . $result_token['_token']
            );

            $ch = curl_init();
            curl_setopt( $ch, CURLOPT_URL, $url);
            curl_setopt( $ch, CURLOPT_RETURNTRANSFER, 1);
            curl_setopt( $ch, CURLOPT_FOLLOWLOCATION, 1);
            curl_setopt( $ch, CURLOPT_HTTPHEADER, $data);
            curl_setopt( $ch, CURLOPT_HEADER, 0);

            $result = curl_exec( $ch );

            curl_close($ch);

            if ($result === FALSE) {
                throw new Exception('CURL Error: ' . curl_error($ch), curl_errno($ch));
            }else{
                $obj = json_decode($result);
                return $obj;
            }
        }else{
            $result = null;
        }
    }

    public function get_token()
    {
        $url = 'http://sisdm.semarangkota.go.id/oauth/token';

        $data['grant_type']     = 'password';
        $data['client_id']      = '5';
        $data['client_secret']  = 'gcEBVCjvi6QQRdXtaALozh2zA8gsRnv5fhngTikl';
        $data['username']       = 'bagianorganisasi';
        $data['password']       = 'menjemputrezeki';

        $data_string = '';
        foreach($data as $key => $value) { $data_string .= $key.'='.$value.'&'; }
        rtrim($data_string, '&');
        
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_POST, count($data));
        curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
        curl_setopt($ch, CURLOPT_HEADER, 0);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);

        $responseJson = curl_exec( $ch );

        curl_close($ch);

        $result = json_decode($responseJson);

        if(!isset($result->error))
        {
            $token_type     = $result->token_type;
            $access_token   = $result->access_token;
            $auth           = $token_type.' '.$access_token;

            $respon = ['status' => true, '_token' => $access_token, '_token_type' => $token_type];
        }else{
            $respon = ['status' => false, 'message' => $result->message];
        }
        return $respon;
    }

	public function sso_logout()
    {
        #GET
    	$par = [
                    'url'       => $this->sso_logout,
                    ];

        $res = $this->request_get($par);
        //return $res;
        redirect('Login_Controller');
    }
}
