<?php
class Sop_Controller extends CI_Controller {
	function __construct(){
		parent:: __construct();
		if ($this->session->userdata('nama')=="") {
	 		 redirect('LoginAdmin_Controller');
	 	 }
		$this->load->model('Sop_Model');
		// $this->load->library('pdf');
		setlocale (LC_TIME, 'id_ID');
	}
	function page(){
		$page=$this->uri->segment(3);
		$data['page']=$page;
		if($page=="user"){
			$data['tmp_pt']=$this->Sop_Model->qw("*","user","")->result();
		}elseif($page=="pelaksana"){
			$data['tmp_pt']=$this->Sop_Model->qw("*","pelaksana","")->result();
		}elseif($page=="lihat_odha"){
			$leveling = $this->session->userdata('level');
			if($leveling=='kader')
			{
				$username = $this->session->userdata('nama');
				$data['tmp_pt']=$this->Sop_Model->qw("odha.*","odha, kader","WHERE odha.nik_kader=kader.nik AND kader.kode_kader='$username' ORDER BY odha.nama ASC")->result();
			}elseif($leveling=='admin')
			{
				$data['tmp_pt']=$this->Sop_Model->qw("*","odha","ORDER BY id DESC")->result();
			}
			
		}elseif($page=="tambah_odha"){
			$get_prov = $this->db->select('*')->from('wilayah_provinsi')->get();
	   		$data['provinsi'] = $get_prov->result();
	   		/*$data['path'] = base_url('assets');   
	   		$this->load->view('wilayah_view', $data);*/
			$data['open']='Sop_Controller/simpan_odha';
			$data['kembali']='Sop_Controller/page/lihat_odha';
		}elseif($page=="tambah_materi"){
			$data['open']='Sop_Controller/simpan_materi';
			$data['kembali']='Sop_Controller/page/lihat_materi';
		}elseif($page=="tambah_lab"){
			$data['open']='Sop_Controller/simpan_lab';
		}elseif($page=="tambah_art"){
			$data['open']='Sop_Controller/simpan_art';
		}elseif($page=="tambah_kuisioner"){
			$data['open']='Sop_Controller/simpan_kuisioner';
		}elseif($page=="detail_odha"){
			$data['open']='Sop_Controller/simpan_odha';
			$data['kembali']='Sop_Controller/page/lihat_odha';
		}elseif($page=="detail_materi"){
			$data['kembali']='Sop_Controller/page/lihat_materi';
		}elseif($page=="lihat_pisma2"){
			$data['tmp_pt']=$this->Sop_Model->qw("*","fk_master_pisma","WHERE jenis_pisma='2' ORDER BY id DESC")->result();
		}elseif($page=="lihat_obat"){
			$data['tmp_pt']=$this->Sop_Model->qw("*","obat","ORDER BY nama_obat ASC")->result();
		}elseif($page=="lihat_materi"){
			$data['tmp_pt']=$this->Sop_Model->qw("*","materi","ORDER BY judul ASC")->result();
		}elseif($page=="lihat_mahasiswa"){
			$data['tmp_pt']=$this->Sop_Model->qw("*","msmhs","WHERE kdprodi='11201' ORDER BY nim ASC")->result();
		}elseif($page=="lihat_pembimbing"){
			$data['tmp_pt']=$this->Sop_Model->qw("*","fk_penguji","ORDER BY nama ASC")->result();
		}elseif($page=="lihat_rs"){
			$data['tmp_pt']=$this->Sop_Model->qw("*","master_rs","ORDER BY nama_rs ASC")->result();
		}elseif($page=="lihat_form"){
			$data['tmp_pt']=$this->Sop_Model->qw("*","master_form","ORDER BY id ASC")->result();
		}
		$this->load->view('index',$data);
	}

	function lokasi_file($jenis_file,$id_ijin)
	{
		$getfile=$this->Sop_Model->qw("*","file_peserta","WHERE id_ijin='$id_ijin' AND jenis_file='$jenis_file'")->row_array();
		return $getfile;
	}


	function simpan_peserta_ujian(){

		//lakukan cek
		$waktu = date("Y-m-d H:i:s");
		$id_penjadwalan = $this->input->post('id_penjadwalan');
		$id_peserta = $this->input->post('id_peserta');
		$belakang = $id_peserta;

        //foto
        $datetime=date("YmdHis");
		$config_foto['upload_path'] = './assets/uploads/';
        $config_foto['allowed_types'] = 'jpg|jpeg|png'; 
        $config_foto['file_name'] = 'f'.$belakang.$datetime;
        $config_foto['max_size'] = 2000;
        $filename_foto= $_FILES["foto_bukti"]["name"];
        $tipe_file = $_FILES["foto_bukti"]["type"];
		$file_ext_foto = pathinfo($filename_foto,PATHINFO_EXTENSION);

		$allowedExts = array("jpeg", "jpg", "png");
		$eks = explode(".", $filename_foto);
		$extension = end($eks);

		$size=$_FILES['foto_bukti']['size'];

			//ekstensi salah
			if($extension!='jpg' && $extension!='jpeg' && $extension!='png')
			{
				redirect('Sop_Controller/page/daftar_ujian/'.$id_penjadwalan.'/err_ekstensi');
			}else{
				if($size>2000000)
				{
					redirect('Sop_Controller/page/daftar_ujian/'.$id_penjadwalan.'/err_size');
				}
			}

        $nama_lokasi_foto="f".$belakang.$datetime.".".$file_ext_foto;

        //upload foto
		$this->load->library('upload', $config_foto);
        $this->upload->initialize($config_foto);
        $this->upload->do_upload('foto_bukti');

        $ary=array(
        	'id_kelas'	=>$this->input->post('id_kelas'),
        	'id_peserta'		=>$id_peserta,
        	'id_penjadwalan'	=>$id_penjadwalan,
        	'status' =>0,
        	'bukti_bayar'	=>$nama_lokasi_foto
        	);
        $this->Sop_Model->simpan_peserta_ujian('t_peserta_ujian',$ary);
        redirect('Sop_Controller/page/data_ujian/'.$id_penjadwalan.'/berhasil');

	}

	function simpan_daftar_ujian($id_penjadwalan,$id_peserta,$id_kelas){

		//lakukan cek
		$waktu = date("Y-m-d H:i:s");
		//$id_penjadwalan = $this->input->post('id_penjadwalan');
		$id_peserta = $id_peserta;
		$belakang = $id_peserta;
        //cek harga
        $get_harga=$this->Sop_Model->qw("t_biaya_ujian.biaya","t_biaya_ujian, t_penjadwalan","WHERE t_penjadwalan.id_penjadwalan='$id_penjadwalan' AND t_penjadwalan.jenis_ujian=t_biaya_ujian.jenis_ujian AND t_biaya_ujian.status='1'")->row_array();
        $biaya = $get_harga['biaya'];
        $digits = 2;
        $random = str_pad(rand(0, pow(10, $digits)-1), $digits, '0', STR_PAD_LEFT);
        if($random=='00')
        	$random = str_pad(rand(0, pow(10, $digits)-1), $digits, '0', STR_PAD_LEFT);
        $total_biaya = $biaya + $random;
        
        //cek urutan terakhir
        $get_akhir = $this->Sop_Model->qw("urutan","t_peserta_ujian","WHERE id_penjadwalan='$id_penjadwalan' ORDER BY id_peserta_ujian DESC")->row_array();
        $urutan_terakhir = $get_akhir['urutan'];
        $urutan_sekarang = $urutan_terakhir + 1;
        $get_penjadwalan = $this->Sop_Model->qw("kode_jadwal","t_penjadwalan","WHERE id_penjadwalan='$id_penjadwalan'")->row_array();
        $kode_jadwal = $get_penjadwalan['kode_jadwal'];
        $kode_urutan = sprintf("%03d", $urutan_sekarang);
        $no_reg = $kode_jadwal.$kode_urutan;

        $ary=array(
        	'id_kelas'	=>$id_kelas,
        	'id_peserta'		=>$id_peserta,
        	'id_penjadwalan'	=>$id_penjadwalan,
        	'status' =>0,
        	'bukti_bayar'		=>'',
        	'biaya'		=>$biaya,
        	'random'		=>$random,
        	'total_biaya'		=>$total_biaya,
        	'score'		=>0,
        	'urutan'		=>$urutan_sekarang,
        	'no_reg'		=>$no_reg
        	);
        $this->Sop_Model->simpan_peserta_ujian('t_peserta_ujian',$ary);
        $id_peserta_ujian = $this->db->insert_id();
        redirect('Sop_Controller/page/berhasil_daftar/'.$id_penjadwalan.'/'.$id_peserta_ujian);

	}

	function simpan_upload_bukti(){

		//lakukan cek
		$waktu = date("Y-m-d H:i:s");
		$id_penjadwalan = $this->input->post('id_penjadwalan');
		$id_peserta = $this->input->post('id_peserta');
		$id_peserta_ujian = $this->input->post('id_peserta_ujian');
		$belakang = $id_peserta;

        //foto
        $datetime=date("YmdHis");
		$config_foto['upload_path'] = './assets/uploads/';
        $config_foto['allowed_types'] = 'jpg|jpeg|png|pdf'; 
        $config_foto['file_name'] = 'f'.$belakang.$datetime;
        $config_foto['max_size'] = 2000;
        $filename_foto= $_FILES["foto_bukti"]["name"];
        $tipe_file = $_FILES["foto_bukti"]["type"];
		$file_ext_foto = pathinfo($filename_foto,PATHINFO_EXTENSION);

		$allowedExts = array("jpeg", "jpg", "png");
		$eks = explode(".", $filename_foto);
		$extension = end($eks);

		$size=$_FILES['foto_bukti']['size'];

			//ekstensi salah
			if($extension!='jpg' && $extension!='jpeg' && $extension!='png' && $extension!='pdf')
			{
				redirect('Sop_Controller/page/daftar_ujian/'.$id_penjadwalan.'/'.$id_peserta_ujian.'/err_ekstensi');
			}else{
				if($size>2000000)
				{
					redirect('Sop_Controller/page/daftar_ujian/'.$id_penjadwalan.'/'.$id_peserta_ujian.'/err_size');
				}
			}

        $nama_lokasi_foto="f".$belakang.$datetime.".".$file_ext_foto;

        //upload foto
		$this->load->library('upload', $config_foto);
        $this->upload->initialize($config_foto);
        $this->upload->do_upload('foto_bukti');

        $ary=array(
        	'status' =>2,
        	'bukti_bayar'	=>$nama_lokasi_foto
        	);
        //$this->Sop_Model->simpan_peserta_ujian('t_peserta_ujian',$ary);
        $this->Sop_Model->simpan_upload_bukti('t_peserta_ujian',$id_peserta_ujian,$ary);
        redirect('Sop_Controller/page/data_ujian/'.$id_penjadwalan.'/berhasil');

	}

	function ubah_ta(){
		$ta = $this->input->post('ta');
		redirect('Sop_Controller/page/lihat_mahasiswa_periode/'.$ta.'/'.$semester);
	}


	function ubah_ta_plot(){
		$tahun_masuk = $this->input->post('tahun_masuk');
		$id_kelompok = $this->input->post('id_kelompok');
		redirect('Sop_Controller/page/lihat_plot_mhs_tahsin/'.$id_kelompok.'/'.$tahun_masuk);
	}

	function ubah_ta_peserta(){
		$tahun_masuk = $this->input->post('tahun_masuk');
		$id_pisma = $this->input->post('id_pisma');
		redirect('Sop_Controller/page/detail_pisma2/'.$id_pisma.'/'.$tahun_masuk);
	}

	function simpan_lab(){
		$id_odha = $this->input->post('id_odha');
		$datetime=date("YmdHis");
		$config['upload_path'] = './assets/uploads/';
        $config['allowed_types'] = 'jpg|jpeg|png|pdf'; 
        $config['file_name'] = $datetime;
        $config['max_size'] = 2048;
        $filename= $_FILES["file"]["name"];
		$file_ext = pathinfo($filename,PATHINFO_EXTENSION);

		if($file_ext!='')
			$nama_lokasi=$datetime.".".$file_ext;
		else
			$nama_lokasi='';

        $userid = $this->input->post('userid');

		$ary=array(
			'id_odha'	=>$id_odha,
			'jenis_lab'	=>$this->input->post('jenis_lab'),
			'waktu'	=>$this->input->post('waktu'),
			'waktu_berikutnya'	=>$this->input->post('waktu_berikutnya'),
			'keterangan'	=>$this->input->post('keterangan'),
			'file'	=>$nama_lokasi
			);

		$this->load->library('upload', $config);
        if (!$this->upload->do_upload('file')) {
        	$this->Sop_Model->simpan_lab('lab',$ary);
           	redirect('Sop_Controller/page/lihat_lab/'.$id_odha.'/sudah_input');
        }else{
			$this->Sop_Model->simpan_lab('lab',$ary);
			redirect('Sop_Controller/page/lihat_lab/'.$id_odha.'/sudah_input');
        }
		
	}

	function simpan_tambah_kelompok(){
		$ta = $this->input->post('ta');
		$ta_depan = substr($ta,0,4);
		$kelompok = $this->input->post('kelompok');
		$pembimbing = $this->input->post('pembimbing');
		$ary=array(
			'ta'	=>$ta,
			'kelompok'	=>$this->input->post('kelompok'),
			'tutor'	=>$this->input->post('pembimbing')
			);

		$this->Sop_Model->simpan_kelompok_tahsin('fk_kelompok_tahsin',$ary);
		redirect('Sop_Controller/page/lihat_plot_tutor_tahsin/'.$ta_depan);
	}

	function simpan_ibu(){
		$username = $this->session->userdata('id_user');
		$nama_ibu = $this->input->post('nama_ibu');
		$nama_suami = $this->input->post('nama_suami');
		$umur = $this->input->post('umur');
		$umur_kehamilan = $this->input->post('umur_kehamilan');
		$bidan = $this->input->post('bidan');
		$dokter = $this->input->post('dokter');
		// foreach ($this->input->post('nim') as $key => $value) {
			$ary=array(
			'id_user'	=>$username,
			'nama_ibu'	=>$nama_ibu,
			'nama_suami'	=>$nama_suami,
			'umur'	=>$umur,
			'umur_kehamilan'	=>$umur_kehamilan,
			'bidan'	=>$bidan,
			'dokter'	=>$dokter
			);

			$this->Sop_Model->simpan_ibu('data_ibu',$ary);
		// }

		redirect('Sop_Controller/page/data_ibu/');

	}

	function simpan_screening(){
		$username = $this->session->userdata('id_user');
		$tanggal = $this->input->post('tanggal');
		$tempat = $this->input->post('tempat');
		$berat = $this->input->post('berat');
		$tinggi = $this->input->post('tinggi');
		$id_ibu = $this->input->post('id_ibu');
		$jam = $this->input->post('jam');
		$menit = $this->input->post('menit');
		$waktu = $jam.":".$menit;

		// Waktu pertama
		$status = $this->Sop_Model->qw("waktu","log_input","")->row_array();
		$waktu1 = $status['waktu'];

		// Waktu kedua
		$waktu2 = date("Y-m-d H:i:s");

		// Mengubah waktu ke dalam objek DateTime
		$datetime1 = new DateTime($waktu1);
		$datetime2 = new DateTime($waktu2);

		// Menghitung selisih waktu dalam detik
		$selisihDetik = $datetime2->getTimestamp() - $datetime1->getTimestamp();

		// Menghitung menit dan detik dari selisih detik
		$menit = floor($selisihDetik / 60);
		$detik = $selisihDetik % 60;

		$waktu_lama = $menit." menit ".$detik." detik";

		$ary=array(
		'id_user'	=>$username,
		'tanggal'	=>$tanggal,
		'tempat'	=>$tempat,
		'berat'	=>$berat,
		'id_ibu'	=>$id_ibu,
		'tinggi'	=>$tinggi,
		'waktu'	=>$waktu_lama,
		);

		$this->Sop_Model->simpan_screening('data_screening',$ary);

		$id_screening = $this->db->insert_id();

		$no = 0;

		foreach ($this->input->post('jawaban') as $key => $value) {
			
			$no++;
			
			$ary=array(
			'id_screening'	=>$id_screening,
			'pertanyaan'	=>$no,
			'jawaban'	=>$value
			);

			$this->Sop_Model->simpan_jawaban('data_jawaban',$ary);
		}

		redirect('Sop_Controller/page/data_kesimpulan/'.$id_screening);

	}
	
	function add_ajax_kab($id_prov)
	{
    	$query = $this->db->get_where('wilayah_kabupaten',array('provinsi_id'=>$id_prov));
    	$data = "<option value=''>Pilih Kabupaten</option>";
    	foreach ($query->result() as $value) {
        	$data .= "<option value='".$value->id."'>".$value->nama."</option>";
    	}
    	echo $data;
	}
  
	function add_ajax_kec($id_kab)
	{
    	$query = $this->db->get_where('wilayah_kecamatan',array('kabupaten_id'=>$id_kab));
    	$data = "<option value=''>Pilih Kecamatan</option>";
    	foreach ($query->result() as $value) {
        	$data .= "<option value='".$value->id."'>".$value->nama."</option>";
    	}
    	echo $data;
	}
  
	function add_ajax_des($id_kec)
	{
    	$query = $this->db->get_where('wilayah_desa',array('kecamatan_id'=>$id_kec));
    	$data = "<option value=''>Pilih Desa</option>";
    	foreach ($query->result() as $value) {
        	$data .= "<option value='".$value->id."'>".$value->nama."</option>";
    	}
    	echo $data;
	}

	function edit_user(){
		$id=$this->input->post('id_user');
		$password=md5($this->input->post('password'));
		$ary=array(
			'id_user'	=>$this->input->post('id_user'),
			'username'	=>$this->input->post('username'),
			'password'	=>$password,
			'level'		=>$this->input->post('level')
		);
		$this->Sop_Model->edit_user('user',$id,$ary);
		redirect('Sop_Controller/page/user');
	}

	function ubah_sandi(){
		$username=$this->input->post('username');
		//$password=md5('oraora'.$this->input->post('password'));
		$password=md5($this->input->post('password'));
		$ary=array(
			'password'	=>$password
		);
		$this->Sop_Model->reset_password('user',$username,$ary);
		redirect('Sop_Controller/page/ubah_sandi/sukses');
	}

	function ubah_status_plot($id_odha,$id_plot_obat){
		//ambil status plot obat
		$status = $this->Sop_Model->qw("status","plot_obat","WHERE id='$id_plot_obat'")->row_array();
		if($status['status']=='0')
			$status_baru = 1;
		else
			$status_baru = 0;
		$ary=array(
			'status'	=>$status_baru
		);
		$this->Sop_Model->ubah_status_plot('plot_obat',$id_plot_obat,$ary);
		redirect('Sop_Controller/page/lihat_plot_obat/'.$id_odha);
	}

	function reset_password($nip){
		$password=md5("bkpp2019");
		$ary=array(
			'password'	=>$password
		);
		$this->Sop_Model->reset_password('user',$nip,$ary);
		redirect('Sop_Controller/page/lihat_pengguna/'.$nip);
	}

	function ubah_pejabat(){
		$id=$this->input->post('id');
		$pejabat_baru = $this->input->post('pejabat_baru');
		$pjb = $this->get_pegawai($pejabat_baru);
		if($pjb['status']=='false')
		{
			redirect('Sop_Controller/page/ubah_pejabat/'.$id.'/error');
		}else{
			$ary=array(
			'nip'	=>$pejabat_baru
			);
			$this->Sop_Model->ubah_pejabat('pejabat',$id,$ary);
			redirect('Sop_Controller/page/lihat_pejabat/'.$id);
		}
		
	}
	
	function hapus_odha($id){
		$this->Sop_Model->hapus_odha('odha',$id);
		redirect('Sop_Controller/page/lihat_odha/hapus');
	}

	function hapus_peserta($id,$id_pisma){
		$this->Sop_Model->hapus_peserta('fk_peserta_pisma',$id);
		redirect('Sop_Controller/page/detail_pisma2/'.$id_pisma);
	}

	function hapus_kegiatan($id_pisma,$id){
		$this->Sop_Model->hapus_kegiatan('fk_keg_pisma',$id);
		redirect('Sop_Controller/page/detail_pisma2/'.$id_pisma.'/2');
	}

	function hapus_art($id_odha,$id){
		$this->Sop_Model->hapus_art('plot_obat',$id);
		redirect('Sop_Controller/page/lihat_plot_obat/'.$id_odha.'/hapus');
	}

	function hapus_materi($id){
		$this->Sop_Model->hapus_materi('materi',$id);
		redirect('Sop_Controller/page/lihat_materi/hapus');
	}


	function masainput($tanggal){
		//buat tanggal
		$array_months = ["empty", "Januari", "Februari", "Maret", "April", "Mei", "Juni", "Juli", "Agustus", "September", "Oktober", "November", "Desember"];
		$masainput = date('d', strtotime($tanggal))." ".$array_months[date("n", strtotime($tanggal))]." ".date('Y', strtotime($tanggal));
		return $masainput;
	}

	function get_bulan($bulan){
		//buat bulan
		$input = ltrim($bulan, '0');
		$array_months = ["empty", "Januari", "Februari", "Maret", "April", "Mei", "Juni", "Juli", "Agustus", "September", "Oktober", "November", "Desember"];
		$masainput = $array_months[$input];
		return $masainput;
	}

	function akses_email()
	{
		$daftar_hari = array(
		 'Sunday' => 'Minggu',
		 'Monday' => 'Senin',
		 'Tuesday' => 'Selasa',
		 'Wednesday' => 'Rabu',
		 'Thursday' => 'Kamis',
		 'Friday' => 'Jumat',
		 'Saturday' => 'Sabtu'
		);
		//$this->kirim_email($penerima,$tipe,$pesan);

		//ambil lab
		$lab=$this->Sop_Model->qw("lab.*, odha.nik, odha.nama, odha.email, master_lab.nama_lab","lab, odha, master_lab","
		  WHERE lab.waktu_berikutnya <= NOW() + INTERVAL 3 DAY
		  AND lab.waktu_berikutnya >= NOW()
		  AND lab.id_odha=odha.id
		  AND lab.jenis_lab=master_lab.id
		  AND lab.notifikasi='0'
		  AND odha.notifikasi_lab='1'
		  ORDER BY lab.waktu_berikutnya ASC")->result();

		foreach($lab as $tampil){
			$tanggal_baru = date("d-m-Y",strtotime($tampil->waktu_berikutnya));
			$hari = date('l', strtotime($tampil->waktu_berikutnya));
			$nama_hari = $daftar_hari[$hari];
			$penerima = $tampil->email;
			$tipe = 'lab';
			$pesan = "Mengingatkan bahwa <strong>Jadwal Uji Laboratorium berikutnya</strong> adalah pada <strong>".$nama_hari.", ".$tanggal_baru."</strong> untuk Uji Laboratorium <strong>".$tampil->nama_lab."</strong>.<br>Tetap lakukan Uji Laboratorium sesuai dengan waktu yang ditentukan.<br><br>Terima Kasih<br><strong>APLIKASI SEHAT KADER</strong>";
			$ary=array(
			'notifikasi'	=>1
			);

			echo $pesan;
			$this->Sop_Model->ubah_notifikasi('lab',$tampil->id,$ary);
			$this->kirim_email($penerima,$tipe,$pesan);
			echo "<br>";
		}

		$url1=$_SERVER['REQUEST_URI'];
    	header("Refresh: 15; URL=$url1");
	}

	function unduh_invoice($id_penjadwalan,$id_peserta_ujian)
	{
		$email = $this->session->userdata('email');

		$info=$this->Sop_Model->qw("t_penjadwalan.*, t_kat_soal.kategori, t_biaya_ujian.nama_jenis_ujian","t_kat_soal, t_penjadwalan, t_biaya_ujian","WHERE t_kat_soal.id=t_penjadwalan.tipe_ujian AND t_penjadwalan.id_penjadwalan='$id_penjadwalan' AND t_penjadwalan.jenis_ujian=t_biaya_ujian.jenis_ujian AND t_biaya_ujian.status='1'")->row_array();
		if($info['online_offline']=='1')
			$onoff = "Offline";
		elseif($info['online_offline']=='2')
			$onoff = "Online";
		else $onoff = 'Offline / Online';

		if($info['online_offline_pelatihan']=='1')
			$onoff_p = "Offline";
		elseif($info['online_offline_pelatihan']=='2')
			$onoff_p = "Online";
		else $onoff_p = "Offline / Online";

		$info_user = $this->Sop_Model->qw("tblkelas.ID, tblsiswa.id_siswa, tblsiswa.nim, tblsiswa.nama","tblkelas, tblsiswa","WHERE tblkelas.Kelas=tblsiswa.nis AND tblsiswa.nis='$email'")->row_array();

		$info_bayar = $this->Sop_Model->qw("*","t_peserta_ujian","WHERE id_peserta_ujian='$id_peserta_ujian'")->row_array();

		//tanggal daftar
		$tanggal_daftar = date("d-m-Y",strtotime($info_bayar['created_at']));
		$batas_bayar = date('d-m-Y', strtotime($tanggal_daftar. ' + 3 days')); 

		$logo = "./assets/image/logo_bbj.png";
		$pdf = new FPDF('l','mm','A4');
		$pdf->AddPage('P');
		$pdf->SetLeftMargin(10);
		// mencetak string 
		//$pdf->Cell(10,40,'',0,1);
		$pdf->Image($logo, 10, $pdf->GetY(), 30);
		$pdf->SetFont('Arial','B',16);
		$pdf->SetTextColor(0,76,153);
		$pdf->Cell(35,7,'',0,0);
		$pdf->Cell(80,7,'BINA BAHASA JAYA (BBJ) USM',0,0);
		$pdf->SetFont('Arial','B',16);
		$pdf->Cell(60,7,'INVOICE',0,1,'R');
		$pdf->SetFont('Arial','',10);
		$pdf->Cell(35,3,'',0,0);
		$pdf->Cell(100,3,'Jl. Soekarno-Hatta, Tlogosari, Semarang, Jawa Tengah',0,0);
		$pdf->Cell(60,3,'',0,1,'R');
		$pdf->Cell(35,5,'',0,0);
		$pdf->Cell(160,5,'Telp : (024) 6702757, Whatsapp : 085786792330, Email : bbj@usm.ac.id',0,1);

		//buat garis
		$pdf->SetLineWidth(0.7);
		$pdf->SetDrawColor(96,96,96);
		$pdf->Line(10, 28, 195, 28);
		$pdf->SetLineWidth(0.3);
		$pdf->Line(10, 27, 195, 27);

		$pdf->SetTextColor(0,0,0);
		$pdf->Cell(5,6,'',0,1);
		$pdf->SetFont('Arial','',10);
		$pdf->Cell(5,7,'',0,0);
		$pdf->Cell(30,5,'No. Registrasi',0,0);
		$pdf->Cell(70,5,':  '.$info_bayar['no_reg'].'',0,1);
		$pdf->Cell(5,7,'',0,0);
		$pdf->Cell(30,5,'NIM / NIK',0,0);
		$pdf->Cell(70,5,':  '.$info_user['nim'].'',0,1);
		$pdf->Cell(5,7,'',0,0);
		$pdf->Cell(30,5,'Peserta',0,0);
		//$pdf->Cell(70,5,':  '.$info_user['nama'].' ('.$get['no_mahasiswa2'].')',0,1);
		$pdf->Cell(70,5,':  '.$info_user['nama'].'',0,1);
		/*$pdf->Cell(5,5,'',0,0);
		$pdf->Cell(30,5,'Program Studi',0,0);
		//$pdf->Cell(70,5,':  '.$get['prodi'],0,1);
		$pdf->Cell(5,5,'',0,0);
		$pdf->Cell(30,5,'No. HP',0,0);
		//$pdf->Cell(70,5,':  '.$get['no_telpon2'],0,1);*/
		$pdf->Cell(5,5,'',0,0);
		$pdf->Cell(30,5,'Email',0,0);
		$pdf->Cell(200,5,':  '.$email,0,1);

		/*buat garis
		$pdf->SetLineWidth(0.3);
		$pdf->SetDrawColor(96,96,96);
		$pdf->Line(15, 54, 52, 54);
		$pdf->Line(15, 54, 15, 60);
		$pdf->Line(15, 60, 52, 60);
		$pdf->Line(52, 54, 52, 60);*/

		$pdf->SetTextColor(0,0,0);
		$pdf->Cell(5,4,'',0,1);
		$pdf->Cell(5,4,'',0,0);
		$pdf->SetFont('Arial','B',10);
		$pdf->Cell(5,4,'DETAIL UJIAN',0,1);
		$pdf->SetFont('Arial','',10);
		$pdf->Cell(5,1,'',0,1);
		$pdf->Cell(5,5,'',0,0);
		$pdf->Cell(30,5,'Kode Jadwal',0,0);
		$pdf->Cell(70,5,':  '.$info['kode_jadwal'],0,1);
		$pdf->Cell(5,5,'',0,0);
		$pdf->SetFont('Arial','',10);
		$pdf->Cell(30,5,'Jenis Ujian',0,0);
		$pdf->Cell(70,5,':  '.$info['kategori'].' ('.$info['nama_jenis_ujian'].')',0,1);
		if($info['jenis_ujian']=='1')
		{
			$pdf->Cell(5,5,'',0,0);
			$pdf->Cell(30,5,'Tipe Pelatihan',0,0);
			$pdf->Cell(70,5,':  '.$onoff_p,0,1);
			$pdf->Cell(5,5,'',0,0);
			$pdf->SetFont('Arial','',10);
			$pdf->Cell(30,5,'Waktu Pelatihan',0,0);
			$pdf->Cell(70,5,':  '.date("d-m-Y",strtotime($info['tanggal_pelatihan'])).' ('.$info['pelatihan_mulai'].' - '.$info['pelatihan_selesai'].')',0,1);
		}
		$pdf->Cell(5,5,'',0,0);
		$pdf->Cell(30,5,'Tipe Ujian',0,0);
		$pdf->Cell(70,5,':  '.$onoff,0,1);
		$pdf->Cell(5,5,'',0,0);
		$pdf->SetFont('Arial','',10);
		$pdf->Cell(30,5,'Waktu Ujian',0,0);
		$pdf->Cell(70,5,':  '.date("d-m-Y",strtotime($info['tanggal'])).' ('.$info['waktu_mulai'].' - '.$info['waktu_selesai'].')',0,1);
		$pdf->Cell(5,4,'',0,1);
		$pdf->Cell(5,4,'',0,0);
		$pdf->SetFont('Arial','B',10);
		$pdf->Cell(5,4,'DETAIL BIAYA & PEMBAYARAN',0,1);
		$pdf->SetFont('Arial','',10);
		$pdf->Cell(5,1,'',0,1);
		$pdf->SetFont('Arial','',10);
		$pdf->Cell(5,5,'',0,0);
		$pdf->Cell(30,5,'Waktu Daftar',0,0);
		$pdf->Cell(70,5,':  '.$tanggal_daftar,0,1);
		$pdf->Cell(5,5,'',0,0);
		$pdf->SetFont('Arial','B',10);
		$pdf->SetTextColor(255,0,0);
		$pdf->Cell(30,5,'Batas Bayar',0,0);
		$pdf->Cell(70,5,':  '.$batas_bayar,0,1);
		$pdf->SetTextColor(0,0,0);
		$pdf->Cell(5,5,'',0,0);
		$pdf->SetFont('Arial','',10);
		$pdf->Cell(30,5,'Biaya Ujian',0,0);
		$pdf->Cell(70,5,':  ',0,0);
		$pdf->Cell(60,5,number_format($info_bayar['biaya']),0,1,'R');
		$pdf->Cell(5,5,'',0,0);
		$pdf->Cell(30,5,'Kode Unik',0,0);
		$pdf->Cell(70,5,':  ',0,0);
		$pdf->Cell(60,5,number_format($info_bayar['random']),0,1,'R');

		/*
		$pdf->SetLineWidth(0.3);
		$pdf->SetDrawColor(96,96,96);
		$pdf->Line(145, 104, 176, 104);*/

		$pdf->Cell(5,5,'',0,0);
		$pdf->Cell(30,5,'Total Biaya',0,0);
		$pdf->Cell(70,5,':  ',0,0);
		$pdf->SetFont('Arial','B',14);
		$pdf->Cell(60,8,'Rp. '.number_format($info_bayar['total_biaya']),0,1,'R');
		$pdf->SetFont('Arial','',10);
		$pdf->Cell(5,4,'',0,1);
		$pdf->Cell(35,7,'',0,1);
		$pdf->SetFont('Arial','B',10);
		$pdf->Cell(5,4,'',0,0);
		$pdf->Cell(5,4,'CARA PEMBAYARAN DAN UPLOAD BUKTI PEMBAYARAN',0,1);
		$pdf->SetFont('Arial','',10);
		$pdf->Cell(5,3,'',0,1);
		$pdf->Cell(5,5,'',0,0);
		$pdf->Cell(100,5,'1. Transfer sesuai dengan Total Biaya diatas (termasuk 3 digit terakhir) ke Rekening',0,1);
		$pdf->Cell(5,5,'',0,0);
		$pdf->SetFont('Arial','B',10);
		$pdf->Cell(100,5,'    Bank Rakyat Indonesia (BRI)',0,1);
		$pdf->Cell(5,5,'',0,0);
		$pdf->Cell(100,5,'    1466-01-000003-30-6',0,1);
		$pdf->Cell(5,5,'',0,0);
		$pdf->Cell(100,5,'    A.n YAYASAN ALUMNI UNDIP',0,1);
		$pdf->SetFont('Arial','',10);
		$pdf->Cell(5,5,'',0,0);
		$pdf->Cell(100,5,'2. Simpan Bukti Pembayaran dalam bentuk file gambar (JPG/JPEG/PNG) atau PDF',0,1);
		$pdf->Cell(5,5,'',0,0);
		$pdf->Cell(100,5,'3. Login kembali ke web pendaftaran, masukkan username dan password peserta',0,1);
		$pdf->Cell(5,5,'',0,0);
		$pdf->Cell(100,5,'    masuk pada menu Pendaftaran Ujian, Klik pada tombol "Upload Bukti" pada ujian yang diikuti',0,1);
		$pdf->Cell(5,5,'',0,0);
		$pdf->Cell(100,5,'4. Upload bukti pembayaran dengan klik browse pada bagian bukti bayar',0,1);
		$pdf->Cell(5,5,'',0,0);
		$pdf->Cell(100,5,'    pilih file bukti yang telah disimpan, lalu klik "Upload"',0,1);
		$pdf->Cell(5,5,'',0,0);
		$pdf->Cell(100,5,'5. Setelah itu akan muncul keterangan "Belum Dikonfirmasi", tunggu hingga petugas mengkonfirmasi data Anda',0,1);
		$pdf->Cell(5,5,'',0,0);
		$pdf->Cell(100,5,'6. Setelah dikonfirmasi petugas, akan muncul keterangan "Sudah Dikonfirmasi"',0,1);
		$pdf->Cell(5,5,'',0,0);
		$pdf->Cell(100,5,'7. Selanjutnya klik pada menu "Tata Cara Ujian" untuk dapat mengikuti ujian sesuai jadwal yang ditentukan',0,1);

		$pdf->Output('','Invoice_'.$info_user['nama'].'-'.$info_bayar['id_peserta_ujian'].'.pdf');
	}

	function unduh_kartu($id_penjadwalan,$id_peserta_ujian)
	{
		$email = $this->session->userdata('email');

		$info=$this->Sop_Model->qw("t_penjadwalan.*, t_kat_soal.kategori, t_biaya_ujian.nama_jenis_ujian","t_kat_soal, t_penjadwalan, t_biaya_ujian","WHERE t_kat_soal.id=t_penjadwalan.tipe_ujian AND t_penjadwalan.id_penjadwalan='$id_penjadwalan' AND t_penjadwalan.jenis_ujian=t_biaya_ujian.jenis_ujian AND t_biaya_ujian.status='1'")->row_array();
		if($info['online_offline']=='1')
		{
			$onoff = "Offline";
			$tempat_ujian = "Lab BBJ, Gedung B Lantai 2 (B.2.1.9 / Perpustakaan Lama)";
		}
		elseif($info['online_offline']=='2')
		{
			$onoff = "Online";
			$tempat_ujian = "Via Zoom + Wajib Download Aplikasi BBJ";
		}

		if($info['online_offline_pelatihan']=='1')
			$onoff_p = "Offline";
		elseif($info['online_offline_pelatihan']=='2')
			$onoff_p = "Online";
		else $onoff_p = "Online / Offline";

		$info_user = $this->Sop_Model->qw("tblkelas.ID, tblsiswa.id_siswa, tblsiswa.nama, tblsiswa.nim","tblkelas, tblsiswa","WHERE tblkelas.Kelas=tblsiswa.nis AND tblsiswa.nis='$email'")->row_array();

		$info_bayar = $this->Sop_Model->qw("*","t_peserta_ujian","WHERE id_peserta_ujian='$id_peserta_ujian'")->row_array();
		$logo = "./assets/image/logo_bbj.png";
		$pdf = new FPDF('L','mm','A4');
		$pdf->AddPage('P');
		$pdf->SetLeftMargin(10);
		// mencetak string 
		//$pdf->Cell(10,40,'',0,1);
		$pdf->Image($logo, 10, $pdf->GetY(), 30);
		$pdf->SetFont('Arial','B',16);
		$pdf->SetTextColor(0,76,153);
		$pdf->Cell(35,7,'',0,0);
		$pdf->Cell(80,7,'BINA BAHASA JAYA (BBJ) USM',0,0);
		$pdf->SetFont('Arial','B',16);
		$pdf->Cell(60,7,'KARTU UJIAN',0,1,'R');
		$pdf->SetFont('Arial','',10);
		$pdf->Cell(35,3,'',0,0);
		$pdf->Cell(100,3,'Jl. Soekarno-Hatta, Tlogosari, Semarang, Jawa Tengah',0,0);
		$pdf->Cell(60,3,'',0,1,'R');
		$pdf->Cell(35,5,'',0,0);
		$pdf->Cell(160,5,'Telp : (024) 6702757, Whatsapp : 085786792330, Email : bbj@usm.ac.id',0,1);

		//buat garis
		$pdf->SetLineWidth(0.7);
		$pdf->SetDrawColor(96,96,96);
		$pdf->Line(10, 28, 195, 28);
		$pdf->SetLineWidth(0.3);
		$pdf->Line(10, 27, 195, 27);

		$pdf->SetTextColor(0,0,0);
		$pdf->Cell(5,6,'',0,1);
		$pdf->SetFont('Arial','',10);
		$pdf->Cell(5,7,'',0,0);
		$pdf->Cell(30,5,'No. Registrasi',0,0);
		$pdf->Cell(70,5,':  '.$info_bayar['no_reg'].'',0,1);
		$pdf->Cell(5,7,'',0,0);
		$pdf->Cell(30,5,'NIM / NIK',0,0);
		$pdf->Cell(70,5,':  '.$info_user['nim'].'',0,1);
		$pdf->Cell(5,7,'',0,0);
		$pdf->Cell(30,5,'Peserta',0,0);
		//$pdf->Cell(70,5,':  '.$info_user['nama'].' ('.$get['no_mahasiswa2'].')',0,1);
		$pdf->Cell(70,5,':  '.$info_user['nama'].'',0,1);
		/*$pdf->Cell(5,5,'',0,0);
		$pdf->Cell(30,5,'Program Studi',0,0);
		//$pdf->Cell(70,5,':  '.$get['prodi'],0,1);
		$pdf->Cell(5,5,'',0,0);
		$pdf->Cell(30,5,'No. HP',0,0);
		//$pdf->Cell(70,5,':  '.$get['no_telpon2'],0,1);*/
		$pdf->Cell(5,5,'',0,0);
		$pdf->Cell(30,5,'Email',0,0);
		$pdf->Cell(200,5,':  '.$email,0,1);

		/*buat garis
		$pdf->SetLineWidth(0.3);
		$pdf->SetDrawColor(96,96,96);
		$pdf->Line(15, 54, 52, 54);
		$pdf->Line(15, 54, 15, 60);
		$pdf->Line(15, 60, 52, 60);
		$pdf->Line(52, 54, 52, 60);*/

		$pdf->SetTextColor(0,0,0);
		$pdf->Cell(5,4,'',0,1);
		$pdf->Cell(5,4,'',0,0);
		$pdf->SetFont('Arial','B',10);
		$pdf->Cell(5,4,'DETAIL UJIAN',0,1);
		$pdf->SetFont('Arial','',10);
		$pdf->Cell(5,3,'',0,1);
		$pdf->Cell(5,5,'',0,0);
		$pdf->Cell(30,5,'Kode Jadwal',0,0);
		$pdf->Cell(70,5,':  '.$info['kode_jadwal'],0,1);
		$pdf->Cell(5,5,'',0,0);
		$pdf->SetFont('Arial','',10);
		$pdf->Cell(30,5,'Jenis Ujian',0,0);
		$pdf->Cell(70,5,':  '.$info['kategori'].' ('.$info['nama_jenis_ujian'].')',0,1);
		if($info['jenis_ujian']=='1')
		{
			$pdf->Cell(5,5,'',0,0);
			$pdf->Cell(30,5,'Tipe Pelatihan',0,0);
			$pdf->Cell(70,5,':  '.$onoff_p,0,1);
			$pdf->Cell(5,5,'',0,0);
			$pdf->SetFont('Arial','',10);
			$pdf->Cell(30,5,'Waktu Pelatihan',0,0);
			$pdf->Cell(70,5,':  '.date("d-m-Y",strtotime($info['tanggal_pelatihan'])).' ('.$info['pelatihan_mulai'].' - '.$info['pelatihan_selesai'].')',0,1);
		}
		$pdf->Cell(5,5,'',0,0);
		$pdf->Cell(30,5,'Tipe Ujian',0,0);
		$pdf->Cell(70,5,':  '.$onoff,0,1);
		$pdf->Cell(5,5,'',0,0);
		$pdf->SetFont('Arial','',10);
		$pdf->Cell(30,5,'Waktu Ujian',0,0);
		$pdf->Cell(70,5,':  '.date("d-m-Y",strtotime($info['tanggal'])).' ('.$info['waktu_mulai'].' - '.$info['waktu_selesai'].')',0,1);
		$pdf->SetFont('Arial','',10);
		$pdf->Cell(5,5,'',0,0);
		$pdf->SetFont('Arial','',10);
		$pdf->Cell(30,5,'Tempat Ujian',0,0);
		$pdf->Cell(70,5,':  '.$tempat_ujian,0,1);
		$pdf->SetFont('Arial','',10);
		$pdf->Cell(5,5,'',0,0);
		$pdf->Cell(35,7,'',0,1);
		$pdf->SetFont('Arial','B',10);
		$pdf->Cell(5,4,'',0,0);
		$pdf->Cell(5,4,'Keterangan',0,1);
		$pdf->SetFont('Arial','',10);
		$pdf->Cell(5,5,'',0,0);
		$pdf->SetFont('Arial','B',10);
		$pdf->Cell(100,5,'- Wajib konfirmasi ke Nomor Whatsapp BBJ USM (085786792330) dengan mengirimkan kartu ujian',0,1);
		$pdf->Cell(5,5,'',0,0);
		$pdf->SetFont('Arial','',10);
		$pdf->Cell(100,5,'- Selanjutnya klik pada menu "Tata Cara Ujian" untuk dapat mengikuti ujian sesuai jadwal yang ditentukan',0,1);
		$pdf->Cell(5,10,'',0,1);
		$pdf->Cell(140,4,'',0,0);
		$pdf->Cell(5,4,'Pengawas Ujian',0,1);
		$pdf->Cell(5,20,'',0,1);
		$pdf->Cell(135,4,'',0,0);
		$pdf->Cell(5,4,'(....................................)',0,1);

		$pdf->Output('','Invoice_'.$info_user['nama'].'-'.$info_bayar['id_peserta_ujian'].'.pdf');
	}


	function kirim_email($penerima,$tipe,$pesan)
    {
      // Konfigurasi email
    	//$penerima = 'amranovski@gmail.com';
    	//$tipe = 'lab';
    	//$pesan = 'Obat ARV 2 kali sehari';
    	if($tipe=='lab')
    		$tipe_notif = "Uji Laboratorium";
    	elseif($tipe=='art')
    		$tipe_notif = "Obat ART";
        $config = [
            'mailtype'  => 'html',
            'charset'   => 'utf-8',
            'protocol'  => 'smtp',
            'smtp_host' => 'smtp.gmail.com',
            'smtp_user' => 'sehatkader@gmail.com',  // Email gmail
            'smtp_pass'   => 'sehatkader*5106',  // Password gmail
            'smtp_crypto' => 'ssl',
            'smtp_port'   => 465,
            'crlf'    => "\r\n",
            'newline' => "\r\n"
        ];

        // Load library email dan konfigurasinya
        $this->load->library('email', $config);

        // Email dan nama pengirim
        $this->email->from('no-reply@sehatkader.unimus.ac.id', 'Notifikasi SEHATKADER');

        // Email penerima
        $this->email->to($penerima); // Ganti dengan email tujuan

        // Lampiran email, isi dengan url/path file
        //$this->email->attach('https://masrud.com/content/images/20181215150137-codeigniter-smtp-gmail.png');

        // Subject email
        $this->email->subject('Notifikasi SEHATKADER | '.$tipe_notif);

        // Isi email
        //$this->email->message("Ini adalah contoh email yang dikirim menggunakan SMTP Gmail pada CodeIgniter.<br><br> Klik <strong><a href='https://masrud.com/post/kirim-email-dengan-smtp-gmail' target='_blank' rel='noopener'>disini</a></strong> untuk melihat tutorialnya.");
        $this->email->message($pesan);

        // Tampilkan pesan sukses atau error
        if ($this->email->send()) {
            echo 'Sukses! email berhasil dikirim.';
        } else {
            echo 'Error! email tidak dapat dikirim.';
        }
    }

    function logout(){
		$this->session->sess_destroy();
		redirect('LoginAdmin_Controller');
	}
}
