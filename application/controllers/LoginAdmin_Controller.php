<?php
class LoginAdmin_Controller extends CI_Controller {
	function __construct(){
		parent:: __construct();
		$this->load->model('Login_Model');
	}
	function index(){
		$this->load->view('loginadmin');
	}
	function aksi_login(){
			
			$username=$this->input->post('nip');
			$password=$this->input->post('password');
			$where=array(
					'username' =>$username,
					//'password' =>md5('oraora'.$password)
					//'password' =>$password_en
				);
			$cek=$this->Login_Model->cek_login('user',$where)->num_rows();
			if($cek > 0){
				$query=$this->db->where('username',$username)->get('user');
				$row=$query->row();
				$nama=$row->nama;
				$email=$row->username;
				$id_user=$row->id_user;
				$hasil_pass=$row->password;
				if(password_verify($password, $hasil_pass))
				{
					$data_session=array(
						'nama'	=>$nama,
						'email' =>$email,
						'id_user' =>$id_user
						);
					$this->session->set_userdata($data_session);
					redirect('Sop_Controller/page/data_ibu');
				}else{
					redirect('LoginAdmin_Controller/index/error');
				}
			}else{
				redirect('LoginAdmin_Controller/index/error');
			}
		}
	function logout(){
		$this->session->sess_destroy();
		redirect('LoginAdmin_Controller');
	}
}
