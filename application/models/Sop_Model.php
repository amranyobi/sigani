<?php
	class Sop_Model extends CI_Model{
		function qw($cel,$table,$prop){
			return $this->db->query("SELECT $cel FROM $table $prop");
		}
		function simpan_user($table,$value){
			return $this->db->insert($table,$value);
		}
		function simpan_ibu($table,$value){
			return $this->db->insert($table,$value);
		}
		function simpan_screening($table,$value){
			return $this->db->insert($table,$value);
		}
		function simpan_jawaban($table,$value){
			return $this->db->insert($table,$value);
		}
		function simpan_materi($table,$value){
			return $this->db->insert($table,$value);
		}
		function simpan_tambah_pertemuan($table,$value){
			return $this->db->insert($table,$value);
		}
		function simpan_peserta_ujian($table,$value){
			return $this->db->insert($table,$value);
		}
		function simpan_log($table,$value){
			return $this->db->insert($table,$value);
		}
		function simpan_dimensi($table,$value){
			return $this->db->insert($table,$value);
		}
		function simpan_komponen_nilai($table,$value){
			return $this->db->insert($table,$value);
		}
		function simpan_tambah_kegiatan($table,$value){
			return $this->db->insert($table,$value);
		}
		function simpan_master_jawaban($table,$value){
			return $this->db->insert($table,$value);
		}
		function simpan_tambah_paket($table,$value){
			return $this->db->insert($table,$value);
		}
		function simpan_tambah_pisma2($table,$value){
			return $this->db->insert($table,$value);
		}
		function simpan_presensi_pisma2($table,$value){
			return $this->db->insert($table,$value);
		}
		function edit_odha($table,$where,$value){
			$this->db->where('id',$where);
			return $this->db->update($table,$value);
		}
		function ubah_notifikasi($table,$where,$value){
			$this->db->where('id',$where);
			return $this->db->update($table,$value);
		}
		function reset_password($table,$where,$value){
			$this->db->where('username',$where);
			return $this->db->update($table,$value);
		}
		function edit_status($table,$where,$value){
			$this->db->where('id',$where);
			return $this->db->update($table,$value);
		}
		function simpan_upload_bukti($table,$where,$value){
			$this->db->where('id_peserta_ujian',$where);
			return $this->db->update($table,$value);
		}
		function edit_ujian($table,$where,$value){
			$this->db->where('id',$where);
			return $this->db->update($table,$value);
		}
		function edit_materi($table,$where,$value){
			$this->db->where('id',$where);
			return $this->db->update($table,$value);
		}
		function ubah_status_plot($table,$where,$value){
			$this->db->where('id',$where);
			return $this->db->update($table,$value);
		}
		function ubah_kegiatan($table,$where,$value){
			$this->db->where('id',$where);
			return $this->db->update($table,$value);
		}
		function data_pembimbing($table,$where){
			$this->db->where('data_pembimbing',$where);
			return $this->db->delete($table);
		}
		function hapus_kegiatan($table,$where){
			$this->db->where('id',$where);
			return $this->db->delete($table);
		}
		function hapus_peserta($table,$where){
			$this->db->where('id',$where);
			return $this->db->delete($table);
		}
		function hapus_art($table,$where){
			$this->db->where('id',$where);
			return $this->db->delete($table);
		}
		function hapus_materi($table,$where){
			$this->db->where('id',$where);
			return $this->db->delete($table);
		}

	}
?>
